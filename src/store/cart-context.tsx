import React, { useState } from "react";
import { Dayjs } from "dayjs";
import { IProduct } from "../../shared/interfaces/IProduct";
import { message } from "antd";
import { CartProduct } from "../interfaces/cart-product.interface";
import { useTranslation } from "react-i18next";

export const CartContext = React.createContext<{
  products: CartProduct[];
  timespan: [Dayjs, Dayjs] | null;
  sum: number;
  addProduct: (product: CartProduct) => void;
  removeProduct: (product: CartProduct) => void;
  setAmount: (product: CartProduct) => void;
  setTimeSpan: (timespan: [Dayjs, Dayjs] | null) => void;
  clear: () => void;
}>({
  products: [],
  timespan: null,
  sum: 0,
  addProduct: (_product: CartProduct) => {},
  removeProduct: (_product: IProduct) => {},
  setAmount: (_product: CartProduct) => {},
  setTimeSpan: (_timespan: [Dayjs, Dayjs] | null) => {},
  clear: () => {},
});

const CartContextProvider: React.FC<any> = ({ children }) => {
  const [cartitems, setCartItems] = useState<CartProduct[]>([]);
  const [timespan, setTimeSpan] = useState<[Dayjs, Dayjs] | null>(null);
  const [messageAPI, contextHolder] = message.useMessage();
  const { t } = useTranslation();

  const addProductHandler = (product: CartProduct) => {
    if (
      cartitems.findIndex((item) => item.ProductId === product.ProductId) !=
        -1 &&
      cartitems.length != 0
    ) {
      messageAPI.error(t("cart.error_duplicate"));
    } else {
      setCartItems((items) => [...items, product]);
      messageAPI.success(`${product.Name} ${t("cart.added")}`);
    }
  };

  const removeProductHandler = (product: IProduct) => {
    if (cartitems.length === 1) {
      setTimeSpan(null);
    }
    setCartItems((items) =>
      items.filter((item) => product.ProductId != item.ProductId)
    );
    messageAPI.success(`${product.Name} ${t("cart.removed")}`);
  };

  const setTimeSpanHandler = (timespan: [Dayjs, Dayjs] | null) => {
    setTimeSpan(timespan);
  };

  const setAmountHandler = (product: CartProduct) => {
    setCartItems((prev) => [
      ...prev.filter((item) => item.ProductId != product.ProductId),
      product,
    ]);
  };

  const sum = cartitems.reduce(
    (accumulator, current) => accumulator + current.RentingFee * current.amount,
    0
  );

  const clearHandler = () => {
    setCartItems([]);
    setTimeSpan(null);
  };

  const contextvalue = {
    products: cartitems.sort(
      (a, b) => Number(a.ProductId) - Number(b.ProductId)
    ),
    timespan: timespan,
    sum: sum,
    addProduct: addProductHandler,
    removeProduct: removeProductHandler,
    setTimeSpan: setTimeSpanHandler,
    setAmount: setAmountHandler,
    clear: clearHandler,
  };

  return (
    <CartContext.Provider value={contextvalue}>
      {contextHolder}
      {children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
