import { useTranslation } from "react-i18next";
import PsaCheckOverviewList from "../components/PsaCheckOverviewList";
import { Divider } from "antd";

const PSA = () => {
  const { t } = useTranslation();

  return (
    <>
      <div className="flex justify-between md:grid grid-cols-3 items-center text-center">
        <h1 className="text-gray-800 col-start-2">{t("general.psa")}</h1>
      </div>
      <Divider className="mt-0"></Divider>
      <PsaCheckOverviewList></PsaCheckOverviewList>
    </>
  );
};

export default PSA;
