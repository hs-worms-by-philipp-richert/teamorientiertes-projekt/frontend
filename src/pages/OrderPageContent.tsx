import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { IOrder } from "../../shared/interfaces/IOrder";
import {
  Button,
  Descriptions,
  DescriptionsProps,
  Divider,
  Dropdown,
  Empty,
  Image,
  MenuProps,
  Modal,
  Skeleton,
  Space,
  Table,
  TableColumnsType,
  Tag,
} from "antd";
import {
  HistoryOutlined,
  CloudDownloadOutlined,
  StopOutlined,
  CheckOutlined,
  FieldTimeOutlined,
  InboxOutlined,
  FilePdfOutlined,
  CarryOutFilled,
  ArrowLeftOutlined,
  DownOutlined,
} from "@ant-design/icons";
import { OrderStatus } from "../../shared/enums/order-status.enum";
import { Link, useLocation } from "react-router-dom";
import { orderAPI } from "../api/OrderAPI";
import { ILineItem } from "../../shared/interfaces/ILineItem";
import { format } from "date-fns";
import { palette } from "../helper/status-palette";
import { IProduct } from "../../shared/interfaces/IProduct";
import { IProductImage } from "../../shared/interfaces/IProductImage";
import { pdfAPI } from "../api/PdfAPI";
import { fileAPI } from "../api/FileAPI";
import { orderTable } from "../interfaces/oder-table.interface";
import { dateTimeFormator, formatPrice } from "../IntlFormat";
import dayjs from "dayjs";
import { FALLBACKIMG } from "../constants/fallbackimage";

const OrderPageContent = () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [orderData, setOrderData] = useState<IOrder>();
  const [totalSum, setTotalSum] = useState(0);
  const [isOpen, setIsOpen] = useState(true);
  const [readyPickup, setReadypickup] = useState(false);
  const [stornieren, setStornieren] = useState(false);
  const [inRental, setInRental] = useState(false);
  const [allReturned, setAllReturned] = useState(true);
  const [collectionReady, setCollectionReady] = useState(false);
  const [picked, setPicked] = useState(false);
  const [data, setDataValues] = useState<orderTable[]>([]);
  const [layout, setLayout] = useState<"vertical" | "horizontal">("horizontal");
  const pathname = useLocation().pathname;
  const orderId = pathname.slice(30);

  const statusIcons = [
    <InboxOutlined />,
    <CheckOutlined />,
    <HistoryOutlined />,
    <CarryOutFilled />,
    <FieldTimeOutlined />,
    <CloudDownloadOutlined />,
    <StopOutlined />,
  ];

  const order_userInfos: DescriptionsProps["items"] = [
    {
      key: "1",
      span: { xs: 1, sm: 1 },
      label: t("userForm.name1"),
      children: orderData?.User?.Firstname,
    },
    {
      key: "2",
      label: t("userForm.name2"),
      span: { xs: 1, sm: 1 },
      children: orderData?.User?.Lastname,
    },
    {
      key: "5",
      span: { xs: 1, sm: 1 },
      label: t("general.email"),
      children: orderData?.User?.Email,
    },
    {
      key: "6",
      label: t("general.shein"),
      span: { xs: 1, sm: 1, md: 1, lg: 1, xl: 1, xxl: 3 },
      children: (
        <Tag icon={<FilePdfOutlined />} className="hover:cursor-pointer">
          <a target="_blanc" href={pdfAPI.getPdf(BigInt(orderId))}>
            {`Order${orderData?.UserId}_${orderData?.OrderDate}_${orderData?.OrderId}`}
          </a>
        </Tag>
      ),
    },
    {
      key: "3",
      span: { xs: 1, sm: 1 },
      label: t("general.order_date"),
      children:
        dayjs(orderData?.OrderDate.toString()).format("DD.MM.YYYY") ?? "",
    },
    {
      key: "7",
      span: { xs: 1, sm: 1 },
      label: t("general.order_satus"),
      children: (
        <Tag
          color={palette[orderData?.OrderStatus || 0]}
          className="text-center md:font-medium"
          key={0}
        >
          <h4 className="flex gap-1 px-5">
            {statusIcons[orderData?.OrderStatus || 0]}
            {t(`general.${OrderStatus[orderData?.OrderStatus || 0]}`)}
          </h4>
        </Tag>
      ),
    },
  ];

  const columns: TableColumnsType<orderTable> = [
    {
      title: t("general.prepared"),
      dataIndex: "prepared",
    },
    {
      title: t("general.end"),
      dataIndex: "Ende",
    },
    {
      title: t("general.start"),
      dataIndex: "Start",
    },
    {
      title: t("general.article"),
      dataIndex: "Artikel",
    },
    {
      title: t("general.image"),
      dataIndex: "Bild",
    },
  ];
  const columns2: TableColumnsType<orderTable> = [
    {
      title: t("general.image"),
      dataIndex: "Bild",
    },
    {
      title: t("general.article"),
      dataIndex: "Artikel",
    },
    {
      title: t("general.start"),
      dataIndex: "Start",
    },
    {
      title: t("general.end"),
      dataIndex: "Ende",
    },
  ];

  const columns3: TableColumnsType<orderTable> = [
    {
      title: t("general.image"),
      dataIndex: "Bild",
    },
    {
      title: t("general.article"),
      dataIndex: "Artikel",
    },
    {
      title: t("general.start"),
      dataIndex: "Start",
    },
    {
      title: t("general.end"),
      dataIndex: "Ende",
    },
    {
      title: t("general.return"),
      dataIndex: "return",
    },
  ];

  const columns4: TableColumnsType<orderTable> = [
    {
      title: t("general.image"),
      dataIndex: "Bild",
    },
    {
      title: t("general.article"),
      dataIndex: "Artikel",
    },
    {
      title: t("general.start"),
      dataIndex: "Start",
    },
    {
      title: t("general.end"),
      dataIndex: "Ende",
    },
    {
      title: t("general.return_date"),
      dataIndex: "returnDate",
    },
  ];

  const setData = (lineItem: ILineItem[]) => {
    const datas: orderTable[] = [];
    for (const i in lineItem) {
      const startDate = new Date(lineItem[i].StartDate);
      const endDate = new Date(lineItem[i].EndDate);
      setAllReturned(
        (prevValue) => prevValue && (lineItem[i].ReturnDate ? true : false)
      );
      const items: MenuProps["items"] = [
        {
          label: <p>{t("general.returned")}</p>,
          key: 0,
          onClick: () => handleSetReturnDate(lineItem[i].LineItemId!),
        },
        {
          type: "divider",
        },
        {
          label: <p>{t("general.not_returned")}</p>,
          key: 1,
          onClick: () => handleSetReturnDateToNull(lineItem[i].LineItemId!),
        },
      ];
      const value: orderTable = {
        key: i,
        Bild: (
          <Image
            className="flex justify-center object-scale-down"
            src={getImageUrl(lineItem[i])}
            height={70}
            fallback={FALLBACKIMG}
          />
        ),
        Artikel: lineItem[i].Item?.Product?.Name || "",
        Start: dateTimeFormator(startDate),
        Ende: dateTimeFormator(endDate),
        prepared: "",
        returnDate:
          format(lineItem[i].ReturnDate || lineItem[i].EndDate, "dd.MM.yyyy") ||
          "",
        return: (
          <Dropdown menu={{ items }} trigger={["click"]}>
            <Space>
              {lineItem[i].ReturnDate ? (
                <Tag
                  color="green"
                  className="text-center md:font-medium"
                  key={0}
                >
                  {t("general.returned")}
                </Tag>
              ) : (
                <Tag
                  color="default"
                  className="text-center md:font-medium"
                  key={1}
                >
                  {t("general.not_returned")}
                </Tag>
              )}
              <DownOutlined />
            </Space>
          </Dropdown>
        ),
      };
      datas.push(value);
    }
    setDataValues(datas);
  };

  const handleSetReturnDate = (lineItemId: bigint) => {
    const putData = async () => {
      try {
        await orderAPI.postReturnDate(lineItemId);
        setAllReturned(true);
        getProduct();
      } catch (error) {
        setLoading(false);
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    putData();
  };

  const handleSetReturnDateToNull = (lineItemId: bigint) => {
    const putData = async () => {
      try {
        await orderAPI.setReturnDateToNull(lineItemId);
        getProduct();
      } catch (error) {
        setLoading(false);
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    putData();
  };

  const getImageUrl = (lineItem: ILineItem) => {
    const product: IProduct | undefined = lineItem.Item?.Product;
    if (product) {
      const productImages: IProductImage[] = product.ProductImages || [];
      const productImageId: string =
        productImages.length > 0 ? productImages[0].ImageId : "";
      const productId: string =
        productImages.length > 0 ? productImages[0].ProductId.toString() : "";
      const finalImageId = fileAPI.getProductImage(
        BigInt(productId),
        productImageId
      );
      return finalImageId;
    }
    return;
  };

  const setCheckValue = (selectedRows: orderTable[]) => {
    if (selectedRows.length == data.length) {
      setReadypickup(true);
    } else {
      setReadypickup(false);
    }
  };

  const autoSetCheckValues = (status: OrderStatus) => {
    if (collectionReady || status >= OrderStatus.ReadyForPickup) {
      setCollectionReady(true);
      setIsOpen(false);
      setInRental(status == OrderStatus.InRental);
      setPicked(status >= OrderStatus.Completed);
    }
  };

  const rowSelection = {
    onChange: (_selectedRowKeys: React.Key[], selectedRows: orderTable[]) => {
      setCheckValue(selectedRows);
    },
    getCheckboxProps: (record: orderTable) => ({
      disabled: collectionReady || !isOpen, // Column configuration not to be checked
      name: record.key.toString(),
    }),
  };

  const getProduct = () => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const order = await orderAPI.getOrder(orderId);
        setOrderData(order.data.Order);
        setTotalSum(order.data.total);
        autoSetCheckValues(Number(order.data.Order.OrderStatus));
        setData(order.data.Order.LineItems);
      } catch (error) {
        setLoading(false);
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    fetchData();
  };

  const handlePickupClik = () => {
    const fetchData = async () => {
      try {
        await orderAPI.changeStatustoPickup(orderId);
        getProduct();
      } catch (error) {
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    fetchData();
  };

  const handleReturnedClik = () => {
    const fetchData = async () => {
      try {
        await orderAPI.changeStatus(orderId, OrderStatus.Completed);
        setIsOpen(false);
        setInRental(true);
        getProduct();
      } catch (error) {
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    fetchData();
  };

  const handlePickedClik = () => {
    const fetchData = async () => {
      try {
        await orderAPI.changeStatus(orderId, OrderStatus.InRental);
        setIsOpen(false);
        getProduct();
      } catch (error) {
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    fetchData();
  };

  const handleOrderCancelation = () => {
    const fetchData = async () => {
      try {
        await orderAPI.changeStatus(orderId, OrderStatus.Canceled);
        setIsOpen(false);
        getProduct();
        setStornieren(false);
      } catch (error) {
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    fetchData();
  };
  const Confirmstonieren = () => {
    setStornieren(true);
  };

  useEffect(() => {
    getProduct();
    updatelayout();
    window.addEventListener("resize", updatelayout);
    return () => window.removeEventListener("resize", updatelayout);
  }, []);

  const updatelayout = () => {
    window.innerWidth < 600 ? setLayout("vertical") : setLayout("horizontal");
  };

  return (
    <>
      {loading && !error ? (
        <>
          <div className="lg:grid grid-cols-3 items-center">
            <div className="text-center lg:text-start">
              <Link to="/dashboard/bestellungen">
                <Button size="large" icon={<ArrowLeftOutlined />}>
                  {t("order_overview_list.navigate_back")}
                </Button>
              </Link>
            </div>
            <h1 className="text-center">{t("general.order_details")}</h1>
          </div>
          <Divider className="mt-0"></Divider>
          <h2>{t("general.client_info")}</h2>
          <div className="md:grid lg:grid-cols-12 gap-10 py-5">
            <Descriptions
              column={{ xs: 1, sm: 1, md: 1, lg: 1, xl: 2, xxl: 3 }}
              className="md:col-span-8 truncate"
              bordered
              layout={layout}
              items={order_userInfos}
            />
          </div>
          <div>
            <div className="md:flex flex-col gap-2">
              <div>
                <h2>{t("general.articles")}</h2>
                <Divider></Divider>
              </div>
              <div className="md:flex-none">
                {isOpen && (
                  <Table
                    columns={columns}
                    className="mr-5"
                    scroll={{ x: 500 }}
                    dataSource={data}
                    rowSelection={{
                      type: "checkbox",
                      ...rowSelection,
                      columnWidth: "80px",
                    }}
                    pagination={{
                      position: ["bottomRight"],
                      style: { float: "right" },
                    }}
                    style={{ direction: "rtl" }}
                  />
                )}
                {!isOpen && !inRental && !picked && (
                  <Table
                    scroll={{ x: 500 }}
                    columns={columns2}
                    dataSource={data}
                  />
                )}
                {!isOpen && inRental && (
                  <Table
                    scroll={{ x: 500 }}
                    columns={columns3}
                    dataSource={data}
                  />
                )}
                {!isOpen && picked && (
                  <Table
                    scroll={{ x: 500 }}
                    columns={columns4}
                    dataSource={data}
                  />
                )}
                <div className="md:float-right mr-10">
                  <h3>{`${t("checkout_overview.sum")}:  ${formatPrice(
                    totalSum
                  )}`}</h3>
                </div>
                <div className="flex flex-row flex-wrap gap-3 mt-12 justify-center">
                  {isOpen && !collectionReady && (
                    <Button
                      danger
                      icon={<StopOutlined />}
                      onClick={Confirmstonieren}
                    >
                      {t("general.order_cancel")}
                    </Button>
                  )}
                  {isOpen && !collectionReady && readyPickup && (
                    <Button
                      type="primary"
                      icon={statusIcons[OrderStatus.ReadyForPickup]}
                      onClick={handlePickupClik}
                    >
                      {t(`general.${OrderStatus[OrderStatus.ReadyForPickup]}`)}
                    </Button>
                  )}
                  {isOpen && !collectionReady && !readyPickup && (
                    <Button
                      type="primary"
                      disabled
                      icon={statusIcons[OrderStatus.ReadyForPickup]}
                    >
                      {t(`general.${OrderStatus[OrderStatus.ReadyForPickup]}`)}
                    </Button>
                  )}
                </div>
                {!(picked || isOpen) && !inRental && (
                  <div className="flex flex-row flex-wrap gap-3">
                    <Button
                      danger
                      icon={<StopOutlined />}
                      onClick={Confirmstonieren}
                    >
                      {t("general.order_cancel")}
                    </Button>
                    <Button type="primary" onClick={handlePickedClik}>
                      {t(`general.picked`)}
                    </Button>
                  </div>
                )}
                {inRental && (
                  <div className="flex flex-row flex-wrap gap-3">
                    <Button
                      danger
                      icon={<StopOutlined />}
                      onClick={Confirmstonieren}
                    >
                      {t("general.order_cancel")}
                    </Button>

                    {allReturned && (
                      <Button type="primary" onClick={handleReturnedClik}>
                        {t(`general.complete`)}
                      </Button>
                    )}
                    {!allReturned && (
                      <Button
                        type="primary"
                        disabled
                        onClick={handlePickedClik}
                      >
                        {t(`general.complete`)}
                      </Button>
                    )}
                  </div>
                )}
              </div>
              <Modal
                centered
                title={t("general.order_cancel")}
                open={stornieren}
                okType="danger"
                okText={t("general.order_cancel")}
                onCancel={() => {
                  setStornieren(false);
                }}
                onOk={handleOrderCancelation}
              >
                <p className="fonnt-medium">{t("general.order_stornieren")}</p>
              </Modal>
            </div>
          </div>
        </>
      ) : !error ? (
        <div>
          <Skeleton paragraph={{ rows: 7 }} active />
        </div>
      ) : (
        <Empty />
      )}
    </>
  );
};

export default OrderPageContent;
