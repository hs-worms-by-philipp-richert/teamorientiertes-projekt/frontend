import { useTranslation } from "react-i18next";
import { Button, Modal, Form, Input, Divider, Popconfirm, message } from "antd";
import { userAPI } from "../api/UserAPIs";
import { useEffect, useState } from "react";
import { IUser } from "../../shared/interfaces/IUser";
import { getUserData } from "../util/auth";
import EditUserForm from "../components/modal/UserEditFormModal";
import { Permission } from "../../shared/enums/permissions.enum";

const SettingsPage = () => {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [userData, setUserData] = useState<IUser>();
  const [isUserEditOpen, setIsUserEditOpen] = useState(false);
  const [messageAPI, contextHolder] = message.useMessage();

  const updateUserData = async (newUserData: IUser) => {
    try {
      setIsUserEditOpen(false);

      if (!userData) return;
      if (!newUserData.Password) delete newUserData.Password;

      await userAPI.putUserSelf(newUserData);
      messageAPI.success(t("success.user_data_changed"));
      await fetchUser();
    } catch (error) {
      const errorMessage = t("error.something_went_wrong");
      messageAPI.error(errorMessage);
    }
  };

  const handleCancel = () => {
    setIsUserEditOpen(false);
  };

  const fetchUser = async () => {
    try {
      const userJWT: IUser = await getUserData();
      const user = await userAPI.getUserById(userJWT.UserId!);

      setUserData(user);
      form.setFieldsValue({
        firstname: user?.Firstname,
        lastname: user?.Lastname,
        mail: user?.Email,
        street: user?.Street,
        postal: user?.Postal,
        city: user?.City,
        country: user?.Country,
        housenumber: user?.Housenumber,
      });
    } catch (error) {
      console.error(error || t("general.get_categories_error"));
    }
  };

  const sendInternalPermissionsRequest = async () => {
    try {
      await userAPI.requestInternalPermissions();
      messageAPI.success(t("success.request_permission"));
    } catch (error) {
      const errorMessage = t("error.something_went_wrong");
      messageAPI.error(errorMessage);
    }
  };

  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <>
      {contextHolder}
      <div className="flex flex-col w-full justify-items-center items-center">
        <h1 className="text-gray-800 col-start-2">{t("settings.title")}</h1>
        <Divider className="mb-0"></Divider>
        <div className="w-full flex max-sm:flex-col md:flex-row md:px-20 py-5">
          <div className="flex flex-col md:w-3/4 lg:w-4/6 xl:w-4/6 max-sm:w-full max-sm:items-center md:items-left px-5">
            <div className="flex flex-col items-left md:w-3/5 max-sm:w-auto">
              <h2 className="text-left">{t("settings.user_info")}</h2>
              <div className="w-auto">
                <Form form={form} layout="vertical" disabled>
                  <Form.Item
                    name="firstname"
                    label={t("userForm.name1")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input placeholder={t("userForm.name1")} />
                  </Form.Item>
                  <Form.Item
                    name="lastname"
                    label={t("userForm.name2")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input placeholder={t("userForm.name2")} />
                  </Form.Item>
                  <Form.Item
                    name="street"
                    label={t("userForm.street")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    name="housenumber"
                    label={t("userForm.house_number")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input placeholder={t("userForm.house_number")} />
                  </Form.Item>
                  <Form.Item
                    name="postal"
                    label={t("userForm.zip_code")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input placeholder={t("userForm.zip_code")} />
                  </Form.Item>
                  <Form.Item
                    name="city"
                    label={t("userForm.city")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    name="country"
                    label={t("userForm.country")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input placeholder={t("userForm.country")} />
                  </Form.Item>
                  <Form.Item
                    name="mail"
                    label={t("userForm.mail")}
                    rules={[{ required: false, message: "example" }]}
                  >
                    <Input placeholder={t("userForm.mail")} />
                  </Form.Item>
                </Form>
              </div>
              <Button
                className="max-[350]:w-auto max-md:w-42 lg:w-48 xl:w-52 max-sm:w-auto md:w-auto"
                type="primary"
                onClick={() => setIsUserEditOpen(!isUserEditOpen)}
              >
                {t("buttons.edit")}
              </Button>
            </div>
            <Modal
              centered
              destroyOnClose
              open={isUserEditOpen}
              okButtonProps={{
                type: "primary",
                form: "userEditForm",
                htmlType: "submit",
              }}
              onCancel={handleCancel}
              cancelText={t("buttons.cancel")}
              width={"80%"}
              okText={t("buttons.save_changes")}
            >
              <EditUserForm updateUser={updateUserData}></EditUserForm>
            </Modal>
          </div>
          {userData && userData.Role.Permission == Permission.Member ? (
            <>
              <div className="flex flex-col lg:w-2/4 xl:w-2/6 max-sm:w-full max-sm:items-center max-sm:align-center max-sm:justify-items-center px-5">
                <h2>{t("settings.are_you_internal")}</h2>
                <p>{t("settings.request_internal_permissions")}</p>
                <Popconfirm
                  title={t("popup.are_you_sure")}
                  description={t("popup.apply_for_internal_rights")}
                  cancelText={t("buttons.cancel")}
                  okText={t("buttons.send_request")}
                  onConfirm={() => sendInternalPermissionsRequest()}
                >
                  <Button
                    type="primary"
                    className="max-[350]:w-auto max-md:w-42 lg:w-48 xl:w-52 max-sm:w-auto md:w-auto"
                  >
                    {t("settings.request_permissions")}
                  </Button>
                </Popconfirm>
              </div>
            </>
          ) : (
            ""
          )}
        </div>
      </div>
    </>
  );
};

export default SettingsPage;
