import { useContext } from "react";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Card, Form, Input, Divider } from "antd";
import { useTranslation } from "react-i18next";
import { Link, useNavigate } from "react-router-dom";
import { AuthContext } from "../store/auth-context";

const Login = () => {
  const { t } = useTranslation();

  const authctx = useContext(AuthContext);
  const navigate = useNavigate();

  const handleLogin = async (logindata: {
    identifier: string;
    password: string;
  }) => {
    const isLoggedIn: boolean = await authctx.login(
      logindata.identifier,
      logindata.password
    );
    if (isLoggedIn) navigate("/");
  };

  return (
    <>
      <div className="py-10 m-auto xl:w-2/6">
        <Card className="p-3 shadow-md">
          <h1 className="text-4xl font-extrabold">{t("login.login_now")}</h1>
          <Divider></Divider>
          <Form
            size="large"
            className="mt-5"
            name="normal_login"
            onFinish={handleLogin}
            initialValues={{ remember: true }}
          >
            <Form.Item name="identifier" rules={[{ required: true }]}>
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder={t("login.memberid_or_mail")}
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: t("register.please_input_password"),
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder={t("general.password")}
              />
            </Form.Item>
            <Form.Item>
              <Button block type="primary" htmlType="submit">
                {t("general.login")}
              </Button>
              <Link to="/register">
                <div className="mt-2">
                  {t("login.or")} <span>{t("login.register_now")}</span>
                </div>
              </Link>
            </Form.Item>
          </Form>
        </Card>
      </div>
    </>
  );
};

export default Login;
