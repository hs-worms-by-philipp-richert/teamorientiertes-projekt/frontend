import { Alert, Button, Divider, Popover, Tag } from "antd";
import { useTranslation } from "react-i18next";
import { Link, useParams } from "react-router-dom";
import { PlusOutlined, ArrowLeftOutlined } from "@ant-design/icons";
import { useState } from "react";
import PsaCheckItem from "../components/PsaCheckItem";
import { IWarehouseStorageTray } from "../../shared/interfaces/IWarehouseStorageTray";

const ItemPSA = () => {
  const { itemId } = useParams();
  const { t } = useTranslation();

  const [itemtitle, setitemtitle]: [string, Function] = useState("");
  const [itemqrcode, setitemqrcode]: [string, Function] = useState("");
  const [itemlocation, setitemlocation]: [
    IWarehouseStorageTray | undefined,
    Function
  ] = useState();
  const [modalOpen, setModalOpen]: [{ showPsaModal: boolean }, Function] =
    useState({ showPsaModal: false });

  const showModal = (showPsaModal: boolean) => {
    setModalOpen({ showPsaModal });
  };

  const setProductItemTitle = (productName: string, qrCode: string) => {
    setitemtitle(productName);
    setitemqrcode(qrCode);
  };

  const setProductItemLocation = (itemLocation: IWarehouseStorageTray) => {
    setitemlocation(itemLocation);
  };

  return (
    <>
      {isFinite(Number(itemId)) ? (
        <div>
          <div className="grid grid-cols-1 md:grid-cols-3 items-start text-center">
            <div className="text-center md:text-start">
              <Link to="/dashboard/psa">
                <Button size="large" icon={<ArrowLeftOutlined />}>
                  {t("psa_check.navigate_back")}
                </Button>
              </Link>
            </div>
            <div>
              <h1 className="text-gray-800 md:col-start-2">
                {t("general.psa")}
              </h1>
              <h2 className="text-gray-700 md:col-start-2">{itemtitle}</h2>
              <div className="col-start-2">
                <Tag color="default" title={t("psa_check.qrcode")}>
                  {itemqrcode}
                </Tag>
                <Popover
                  title={itemlocation?.Storage?.Region?.Warehouse?.Name}
                  content={
                    <p>
                      {itemlocation?.Storage?.Region?.Name},{" "}
                      {itemlocation?.Storage?.Name}
                    </p>
                  }
                >
                  <Tag color="default">{itemlocation?.Name}</Tag>
                </Popover>
              </div>
            </div>
            <div className="text-end self-end mt-5">
              <Button
                size="large"
                icon={<PlusOutlined />}
                onClick={() => showModal(true)}
              >
                {t("general.add")}
              </Button>
            </div>
          </div>
          <Divider className="mb-0"></Divider>
          <PsaCheckItem
            modalopen={modalOpen}
            setmodalopen={setModalOpen}
            itemid={BigInt(itemId ?? "")}
            setname={setProductItemTitle}
            setstorage={setProductItemLocation}
          ></PsaCheckItem>
        </div>
      ) : (
        <Alert message={t("psa_check.itemid_invalid")} type="warning" />
      )}
    </>
  );
};

export default ItemPSA;
