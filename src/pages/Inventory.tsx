import { Button, Tabs, TabsProps } from "antd";
import { useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import InventoryOverviewList from "../components/InventoryOverviewList";
import CategoryOverviewList from "../components/CategoryOverviewList";
import { useTranslation } from "react-i18next";

const Inventory = () => {
  const { t } = useTranslation();

  const [modalOpen, setModalOpen] = useState<{
    showCategoryModal: boolean;
    showMaterialModal: boolean;
  }>({ showCategoryModal: false, showMaterialModal: false });
  const [activetab, setActiveTab] = useState<string>("Material");

  const showModal = (
    showCategoryModal: boolean,
    showMaterialModal: boolean
  ) => {
    setModalOpen({ showCategoryModal, showMaterialModal });
  };

  const tabitems: TabsProps["items"] = [
    {
      key: "Material",
      label: t("general.equipment"),
      children: (
        <InventoryOverviewList
          modalopen={modalOpen}
          setmodalopen={setModalOpen}
        ></InventoryOverviewList>
      ),
    },
    {
      key: "Kategorien",
      label: t("general.categories"),
      children: (
        <CategoryOverviewList
          modalopen={modalOpen}
          setmodalopen={setModalOpen}
        ></CategoryOverviewList>
      ),
    },
  ];

  return (
    <>
      <div className="flex justify-between md:grid grid-cols-3 items-center text-center">
        <h1 className="text-gray-800 col-start-2">{t("general.inventory")}</h1>
        <div className="text-end">
          <Button
            size="large"
            onClick={() =>
              activetab === "Material"
                ? showModal(false, true)
                : showModal(true, false)
            }
          >
            <PlusOutlined />
            <div className="hidden sm:inline ml-2">
              {activetab} {t("general.add")}
            </div>
          </Button>
        </div>
      </div>
      <Tabs
        destroyInactiveTabPane
        onChange={(active) => setActiveTab(active)}
        items={tabitems}
        size="large"
      ></Tabs>
    </>
  );
};

export default Inventory;
