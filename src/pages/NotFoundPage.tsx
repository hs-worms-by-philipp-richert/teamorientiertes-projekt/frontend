import { useTranslation } from "react-i18next";
import { Button, Result } from "antd";
import { useNavigate } from "react-router-dom";

const NotFoundPage = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();

  return (
    <>
      <div className="w-100 flex flex-col items-center justify-center">
      <Result
    status="404"
    title="404"
    subTitle={t('error.page_doesn_not_exists')}
    extra={<Button className="green-7" type="primary" onClick={() => navigate('/')}>{t('buttons.back_home')}</Button>}
  />
      </div>
    </>
  );
};

export default NotFoundPage;
