import {
  Button,
  Descriptions,
  DescriptionsProps,
  Divider,
  Empty,
  Image,
  Modal,
  message,
} from "antd";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { ArrowLeftOutlined, PlusOutlined } from "@ant-design/icons";
import ItemInventoryModal from "../components/modal/ItemInventoryModal";
import ItemInventoryOverviewList from "../components/ItemInventoryOverviewList";
import { useTranslation } from "react-i18next";
import { IProductItem } from "../../shared/interfaces/IProductItem";
import { IProduct } from "../../shared/interfaces/IProduct";
import { productItemAPI } from "../api/ProductItemAPI";
import { productAPI } from "../api/ProductAPI";
import { warehouseAPI } from "../api/WarehouseAPI";
import axios from "axios";
import { fileAPI } from "../api/FileAPI";

const ItemInventory = () => {
  const [modalOpen, setModalOpen] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [product, setProduct] = useState<IProduct>();
  const [selectedproductitem, setSelectedProductItem] = useState<
    IProductItem | undefined
  >();
  const [productitems, setproductitems] = useState<IProductItem[]>([]);
  const { productId } = useParams();
  const [messageAPI, contextHolder] = message.useMessage();
  const { t } = useTranslation();

  const fetchItems = async () => {
    try {
      if (productId) {
        setLoading(true);
        const fetchedItems = await productItemAPI.fetchProductItems(productId);
        setproductitems(fetchedItems);
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
      console.error(error || "Fehler beim Abrufen der Artikel.");
    }
  };

  const getProductInfo = async () => {
    try {
      if (productId) {
        setLoading(true);
        const product = await productAPI.fetchProduct(BigInt(productId));
        setProduct(product.data);
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
      console.error(error);
    }
  };

  useEffect(() => {
    fetchItems();
    getProductInfo();
  }, []);

  const fetchTrayInfo = async (trayId: bigint) => {
    try {
      const response = await warehouseAPI.fetchTrayById(trayId);
      return response.data;
    } catch (error) {
      console.error("Error fetching tray info:", error);
      throw error;
    }
  };

  const handleCreateProductItem = async (values: any) => {
    try {
      const trayInfo = await fetchTrayInfo(BigInt(values.TrayId));

      const productitem = {
        ...values,
        DueDatePSA: values.ManufacturingDate.format("YYYY-MM-DD"),
        ProductId: productId,
        Tray: trayInfo ?? undefined,
        TrayId: undefined,
        Archived: false,
      };
      await productItemAPI.postProductItem(productitem);
      messageAPI.success("Item erfolgreich hinzugefügt");
      fetchItems();
      closeModal();
    } catch (error) {
      console.error("Ein Fehler ist aufgetreten:", error);
      messageAPI.error("Ein Fehler ist aufgetreten.");
    }
  };

  const handleEditProductItem = async (values: any) => {
    try {
      if (selectedproductitem?.ItemId != undefined) {
        const productitem = {
          ...values,
        };
        await productItemAPI.updateProductItem(
          selectedproductitem.ItemId,
          productitem
        );
        messageAPI.success("Item erfolgreich hinzugefügt");
        closeModal();
        setSelectedProductItem(undefined);
        fetchItems();
      }
    } catch (error) {
      console.error("Ein Fehler ist aufgetreten:", error);
      messageAPI.error("Ein Fehler ist aufgetreten.");
    }
  };

  const handleDeleteProductItem = async (itemId: bigint) => {
    try {
      await productItemAPI.deleteProductItem(itemId);
      fetchItems();
    } catch (error) {
      if (axios.isAxiosError(error)) {
        messageAPI.error(error.message);
        console.error(error.message);
      } else {
        console.error(error);
      }
    }
  };

  const showEditModal = (selecteditem: IProductItem) => {
    setSelectedProductItem(selecteditem);
    showModal();
  };

  const showModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
    setSelectedProductItem(undefined);
  };

  const productinfo: DescriptionsProps["items"] = [
    {
      key: "manufacturer",
      label: t("general.manufacturer"),
      children: product?.Manufacturer,
    },
    { key: "name", label: t("general.product"), children: product?.Name },
    {
      key: "fee",
      label: t("general.price"),
      children: <div>{product?.RentingFee}€</div>,
    },
  ];

  return (
    <>
      {contextHolder}
      <div className="flex justify-center md:grid grid-cols-3 items-center text-center gap-3">
        <div className="text-start">
          <Link to="/dashboard/inventar">
            <Button size="large" icon={<ArrowLeftOutlined />}>
              {t("item_inventory_overview.navigate_back")}
            </Button>
          </Link>
        </div>
        <h1 className="hidden md:block text-gray-800">{product?.Name}</h1>
      </div>
      <Divider className="m-1"></Divider>
      <div className="flex flex-col lg:flex-row justify-center lg:justify-start gap-8 my-5 items-center lg:p-5 xl:w-1/2">
        <Image
          height={120}
          className="object-scale-down flex-initial"
          src={
            product?.ProductImages?.length
              ? fileAPI.getProductImage(
                  BigInt(productId!),
                  product?.ProductImages![0].ImageId
                )
              : "https://placehold.co/400"
          }
        ></Image>
        <Descriptions
          bordered
          className="w-full"
          layout="vertical"
          items={productinfo}
        ></Descriptions>
      </div>
      <Modal
        destroyOnClose
        open={modalOpen}
        width={"75%"}
        onCancel={closeModal}
        cancelText={t("general.cancel")}
        okText={t(selectedproductitem ? "general.edit" : "general.add")}
        okButtonProps={{
          type: "primary",
          form: "subModalForm",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
      >
        <ItemInventoryModal
          selectedproductitem={selectedproductitem}
          onSubmit={
            selectedproductitem
              ? handleEditProductItem
              : handleCreateProductItem
          }
        />
      </Modal>
      {productitems.length > 0 ? (
        <>
          <div className="flex justify-between items-center">
            <h1 className="text-gray-800">{t("general.associated_items")}</h1>
            <div className="text-end">
              <Button size="large" onClick={showModal}>
                <PlusOutlined />
                <div className="hidden ml-2 md:inline">
                  {t("item_inventory_overview.add_item")}
                </div>
              </Button>
            </div>
          </div>
          <Divider className="mt-0"></Divider>
          <ItemInventoryOverviewList
            loading={loading}
            items={productitems}
            handleEdit={showEditModal}
            handleDelete={handleDeleteProductItem}
          />
        </>
      ) : (
        <div className="p-5 mb-10">
          <Empty
            description={
              <div className="my-3">
                {t("item_inventory_overview.no_items")}
              </div>
            }
          >
            <Button onClick={showModal} type="primary">
              {t("item_inventory_overview.add_item")}
            </Button>
          </Empty>
        </div>
      )}
    </>
  );
};

export default ItemInventory;
