import { useRef } from "react";
import ProductOverviewList from "../components/ProductOverviewList";
import ProductOverviewSidebar from "../components/ProductOverviewSidebar";
import { Layout, Divider } from "antd";
import { RefCall } from "../interfaces/ref-call.interface";

const { Sider, Content } = Layout;

const ProductOverview: React.FC = () => {
  const productOverviewListRef = useRef<RefCall>(null);

  const handleData = (data: string) => {
    productOverviewListRef.current?.callByRef(data);
  };

  return (
    <>
      <Sider className="hidden md:mr-4 md:block" theme="light">
        <ProductOverviewSidebar onCategoryChange={handleData}></ProductOverviewSidebar>
      </Sider>
      <Content className="p-4">
        <div className="md:hidden my-4">
          <ProductOverviewSidebar onCategoryChange={handleData}></ProductOverviewSidebar>
          <Divider></Divider>
        </div>
        <ProductOverviewList ref={productOverviewListRef}></ProductOverviewList>
      </Content>
    </>
  );
};

export default ProductOverview;
