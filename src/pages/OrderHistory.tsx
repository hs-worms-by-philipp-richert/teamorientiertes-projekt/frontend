import { Button, Divider, List, Pagination, Tag, message } from "antd";
import { IOrder } from "../../shared/interfaces/IOrder";
import { useTranslation } from "react-i18next";
import { useEffect, useState } from "react";
import { orderAPI } from "../api/OrderAPI";
import { getUserData } from "../util/auth";
import dayjs from "dayjs";
import { OrderStatus } from "../../shared/enums/order-status.enum";
import {
  HistoryOutlined,
  CloudDownloadOutlined,
  StopOutlined,
  CheckOutlined,
  FieldTimeOutlined,
  InboxOutlined,
  FilePdfOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import { palette } from "../helper/status-palette";
import { pdfAPI } from "../api/PdfAPI";
import axios from "axios";

const statusIcons = [
  <InboxOutlined />,
  <CheckOutlined />,
  <HistoryOutlined />,
  <FieldTimeOutlined />,
  <CloudDownloadOutlined />,
  <StopOutlined />,
];

const Orderhistory = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [orders, setOrders] = useState<IOrder[]>([]);
  const { t } = useTranslation();
  const [page, setpage] = useState<number>(1);
  const [messageAPI, contextHolder] = message.useMessage();
  const [totallength, settotallength] = useState<number>(0);
  const defaultPageSize = 30;

  const fetchOrders = async (
    page: number = 1,
    pageSize: number = defaultPageSize
  ) => {
    if (getUserData().UserId) {
      try {
        setLoading(true);
        const response = await orderAPI.getOrderByUser(
          getUserData().UserId,
          page,
          pageSize
        );
        setLoading(false);
        settotallength(response.totalLength);
        setOrders(response.data);
      } catch (error) {
        setLoading(false);
        if (axios.isAxiosError(error)) {
          messageAPI.error(error.message);
          console.error(error.message);
        } else {
          console.error(error);
        }
      }
    }
  };

  useEffect(() => {
    fetchOrders();
  }, []);

  return (
    <div className="w-full">
      {contextHolder}
      <h1 className="text-gray-800 text-center">{t("order_history.title")}</h1>
      <Divider className="hidden md:block"></Divider>
      <List
        itemLayout="vertical"
        dataSource={orders}
        header={
          <div className="hidden text-center md:grid grid-cols-4 bg-neutral-50 font-bold">
            <p>{t("order_overview_list.table.order_num")}</p>
            <p>{t("order_overview_list.table.date")}</p>
            <p>{t("order_overview_list.table.status")}</p>
            <p>{t("general.shein")}</p>
            <Divider className="lg:col-span-4 m-0"></Divider>
          </div>
        }
        renderItem={(item, index) => (
          <List.Item key={index}>
            <div className="grid grid-cols-1 md:grid-cols-4 content-center items-center hover:bg-neutral-50 px-3 text-center md:text-start p-2">
              <p className="px-3 font-[FiraSansBold] text-center">
                #{item.OrderId?.toString()}
              </p>
              <p className="font-semibold text-center bg-gray-100 md:bg-transparent">
                <CalendarOutlined className="mr-2 text-gray-800" />
                {dayjs(item.OrderDate).format("DD.MM.YYYY")}
              </p>
              <div className="flex justify-evenly flex-wrap gap-5">
                <Tag
                  className="px-5 py-1"
                  icon={statusIcons[item.OrderStatus]}
                  color={palette[item.OrderStatus]}
                >
                  {t(`general.${OrderStatus[item.OrderStatus || 0]}`)}
                </Tag>
              </div>
              <div className="text-center mt-4 lg:mt-0">
                <Button icon={<FilePdfOutlined />}>
                  <a
                    target="_blanc"
                    href={pdfAPI.getPdf(BigInt(item.OrderId!))}
                  >
                    {t("order_history.show_receipt")}
                  </a>
                </Button>
              </div>
            </div>
          </List.Item>
        )}
      ></List>
      {!loading && orders && orders.length > 0 && (
        <div className="flex justify-center mt-8">
          <Pagination
            onChange={(page, pageSize) => {
              fetchOrders(page, pageSize);
              setpage(page);
            }}
            current={page}
            total={totallength}
            pageSize={defaultPageSize}
          />
        </div>
      )}
    </div>
  );
};

export default Orderhistory;
