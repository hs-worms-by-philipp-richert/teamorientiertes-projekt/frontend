import { IProduct } from "../../shared/interfaces/IProduct";

export interface CartProduct extends IProduct {
    amount: number;
}