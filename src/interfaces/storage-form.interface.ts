export interface StorageForm {
    Name: string,
    Street: string,
    City: string,
    Country: string,
    Housenumber: string,
    Postal: string
}