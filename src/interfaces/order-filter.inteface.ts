
export interface OrderFilter {
  searchterm: string;
  sort: "ASC" | "DESC";
  orderDate?: string;
  orderStatus?: string;
}
