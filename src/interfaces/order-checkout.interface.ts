export interface OrderCheckout {
  LineItems: {
    ItemId: bigint;
    StartDate: string;
    EndDate: string;
  }[];
}
