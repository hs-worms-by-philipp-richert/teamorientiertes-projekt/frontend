export interface RefCall {
  callByRef(value: string): void;
}