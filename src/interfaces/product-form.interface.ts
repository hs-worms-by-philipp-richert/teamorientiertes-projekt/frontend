import { IProduct } from "../../shared/interfaces/IProduct";

export interface ProductForm extends Omit<IProduct, 'Categories'> {
    Categories: bigint[];
}