import React from 'react';

export interface orderTable {
  key: React.Key;
  Bild: any;
  Artikel: string;
  Start: string;
  Ende: string;
  prepared: string;
  returnDate?: string;
  return?: any;
}
