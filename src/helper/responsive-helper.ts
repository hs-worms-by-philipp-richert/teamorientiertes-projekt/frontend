const getWindowWidth = () => {
  return (
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth
  );
};

export const getModalWidth = (): string | number | undefined => {
  const windowWidth = getWindowWidth();
  if (windowWidth <= 767) {
    return "100%";
  } else if (windowWidth >= 768 && windowWidth <= 1023) {
    return "50%";
  } else if (windowWidth >= 1024) {
    return "35%";
  }
  return "50%";
};

export const getDividerPixel = (): string => {
  const windowWidth = getWindowWidth();
  if (windowWidth <= 767) {
    return "18px";
  }
  return "24px";
};

export const getSelectWdth = (): number => {
  const windowWidth = getWindowWidth();
  if (windowWidth <= 767) {
    return 120;
  } else if (windowWidth >= 768 && windowWidth <= 1023) {
    return 130;
  } else if (windowWidth >= 1024) {
    return 200;
  }

  return 200;
};
