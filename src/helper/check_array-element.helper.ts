import { orderTable } from '../interfaces/oder-table.interface';

export const ReplaceOrderValue = (array: orderTable[], element: orderTable) => {
  array[Number(element.key)] = element;

  return array;
};
