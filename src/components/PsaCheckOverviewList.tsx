import { useTranslation } from "react-i18next";
import { IProductItem } from "../../shared/interfaces/IProductItem";
import { useEffect, useState } from "react";
import fetchProductItemsWithPsa from "../api/fetchProductItemsWithPsa";
import { Divider, List, Pagination, Skeleton, Tag } from "antd";
import { NavigateFunction, useNavigate } from "react-router-dom";
import Search from "antd/es/input/Search";

const PsaCheckOverviewList = () => {
  const { t } = useTranslation();

  const [psaitems, setpsaitems] = useState<IProductItem[]>([]);
  const [loading, setloading] = useState<boolean>(false);
  const [filterparams, setfilterparams] = useState<{ search: string }>({
    search: "",
  });

  const [totallength, settotallength] = useState<number>(0);
  const [page, setpage] = useState<number>(1);
  const defaultPageSize = 30;

  const navigate: NavigateFunction = useNavigate();

  const fetchPsaList = (
    page: number = 1,
    take: number = defaultPageSize,
    searchStr?: string
  ) => {
    const fetchData = async () => {
      try {
        setloading(true);
        const result = await fetchProductItemsWithPsa(page, take, searchStr);
        settotallength(result.data.totalLength);
        setpsaitems(result.data.list);
        setloading(false);
      } catch (error) {
        setloading(false);
        console.error(error);
      }
    };

    fetchData();
  };

  useEffect(() => {
    fetchPsaList();
  }, []);

  const handleFiltering = (
    searchStr = filterparams.search,
    page: number = 1,
    take: number = defaultPageSize
  ) => {
    setfilterparams({ search: searchStr });
    fetchPsaList(page, take, searchStr);
  };

  const addOneYear = (date: Date): Date => {
    date.setFullYear(date.getFullYear() + 1);
    return date;
  };

  const addExpiryYears = (date: Date, expiresIn: number): Date => {
    date = new Date(date);
    date.setFullYear(date.getFullYear() + expiresIn);
    return date;
  };

  const colorCodePsaCheck = (lastPsa: Date): string => {
    const today = new Date();

    const todayOneMonth = new Date(today);
    todayOneMonth.setMonth(todayOneMonth.getMonth() + 1);

    const nextPsa = addOneYear(new Date(lastPsa));

    if (Number(nextPsa) < Number(today)) return "error";
    else if (Number(nextPsa) < Number(todayOneMonth)) return "warning";
    else return "success";
  };

  const colorCodeExpiry = (manufacturingDate: Date, expiresIn: number) => {
    const today = new Date();

    const todayOneMonth = new Date(today);
    todayOneMonth.setMonth(todayOneMonth.getMonth() + 3);

    const expiryDate = addExpiryYears(new Date(manufacturingDate), expiresIn);

    if (Number(expiryDate) < Number(today)) return "error";
    else if (Number(expiryDate) < Number(todayOneMonth)) return "warning";
    else return "success";
  };

  const expiryText = (manufacturingDate: Date, expiresIn: number) => {
    const dateStr = addExpiryYears(
      manufacturingDate,
      expiresIn
    ).toLocaleDateString();

    if (colorCodeExpiry(manufacturingDate, expiresIn) === "error")
      return `${t("psa_check.item_list.expired_on")} ${dateStr}`;
    else return `${t("psa_check.item_list.expires_on")} ${dateStr}`;
  };

  const handleNavigation = (id: bigint): void => {
    navigate(`${id}`);
  };

  return (
    <>
      <div className="flex flex-row flex-wrap gap-4">
        <Search
          className="lg:w-1/2"
          placeholder={t("general.search")}
          allowClear
          size="large"
          onChange={(event) => handleFiltering(event.target.value)}
          value={filterparams.search}
        />
      </div>
      {!loading ? (
        <List
          header={
            <div className="hidden w-full md:grid md:grid-cols-4 lg:grid-cols-5 items-center gap-x-5 bg-neutral-50 font-bold text-gray-800">
              <h4 className="col-span-1 md:ml-10">
                {t("psa_check.qrcode")}
                <br />
                {t("psa_check.serialnumber")}
              </h4>
              <h4 className="md:col-span-2">
                {t("psa_check.item_list.product")}
              </h4>
              <h4 className="md:col-span-2">
                {t("psa_check.item_list.next_check")}
              </h4>
              <Divider className="md:col-span-5 lg:col-span-5 m-0"></Divider>
            </div>
          }
          dataSource={psaitems}
          renderItem={(item, index) => (
            <List.Item key={index}>
              <div
                onClick={() => handleNavigation(item.ItemId!)}
                className="w-full grid grid-cols-1 md:grid-cols-4 lg:grid-cols-5 text-center md:text-start items-center gap-5 hover:bg-neutral-50 py-3 hover:cursor-pointer"
              >
                <div className="col-span-1 md:ml-10">
                  <div>{item.QrCode}</div>
                  <h4 className="m-0 text-gray-700">
                    {item.Serialnumber ?? ""}
                  </h4>
                </div>
                <div className="col-span-1 md:col-span-2">
                  <div>{item.Product?.Manufacturer}</div>
                  <h4 className="m-0 text-gray-700">{item.Product?.Name}</h4>
                </div>
                <div className="col-span-1 flex justify-center flex-wrap md:flex-nowrap gap-y-2">
                  {item.PSA?.[0] ? (
                    <Tag
                      className="flex-1 text-center"
                      color={colorCodePsaCheck(
                        new Date(item.PSA[0].DateChecked)
                      )}
                      onClick={(e) => e.stopPropagation()}
                    >
                      {addOneYear(
                        new Date(item.PSA[0].DateChecked)
                      ).toLocaleDateString()}
                    </Tag>
                  ) : (
                    <Tag color="warning" onClick={(e) => e.stopPropagation()}>
                      {t("psa_check.not_checked")}
                    </Tag>
                  )}
                  {item.ExpiresInYears && item.ManufacturingDate ? (
                    <Tag
                      color={colorCodeExpiry(
                        item.ManufacturingDate,
                        item.ExpiresInYears
                      )}
                      onClick={(e) => e.stopPropagation()}
                    >
                      {expiryText(item.ManufacturingDate, item.ExpiresInYears)}
                    </Tag>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </List.Item>
          )}
        ></List>
      ) : (
        <div>
          <List.Item>
            <div className="w-full">
              <Skeleton></Skeleton>
              <Skeleton></Skeleton>
            </div>
          </List.Item>
        </div>
      )}

      {!loading && psaitems && psaitems.length > 0 && (
        <div className="flex justify-center mt-8">
          <Pagination
            onChange={(page, pageSize) => {
              handleFiltering(filterparams.search, page, pageSize);
              setpage(page);
            }}
            current={page}
            total={totallength}
            pageSize={defaultPageSize}
          />
        </div>
      )}
    </>
  );
};

export default PsaCheckOverviewList;
