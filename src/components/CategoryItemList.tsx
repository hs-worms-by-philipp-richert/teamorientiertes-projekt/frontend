import { Button, List } from "antd";
import { useEffect, useState } from "react";
import { ICategory } from "../../shared/interfaces/ICategory";
import { IProduct } from "../../shared/interfaces/IProduct";
import { useTranslation } from "react-i18next";
import { categoryAPI } from "../api/CategoryAPI";

const CategoryItemList = ({
  category,
}: {
  category: ICategory | undefined | null;
}) => {
  const { t } = useTranslation();

  const [products, setProducts]: [IProduct[], Function] = useState([]);

  useEffect(() => {
    const getProductsinCategory = async () => {
      if (category?.CategoryId) {
        try {
          const categorydata = await categoryAPI.fetchCategory(
            category.CategoryId
          );
          setProducts(categorydata.data.pagedList.list);
        } catch (error) {
          console.error(error);
        }
      }
    };
    getProductsinCategory();
  }, [category]);

  const handleremoveproduct = async (product: IProduct) => {
    try {
      if (product.ProductId && category?.CategoryId) {
        await categoryAPI.deleteProductFromCategory(
          product?.ProductId,
          category?.CategoryId
        );
        setProducts((products: IProduct[]) =>
          products.filter((item) => item.ProductId != product.ProductId)
        );
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <List
      className="mt-4"
      header={
        <div className="flex w-full items-center gap-5 bg-neutral-50 font-bold px-3">
          <p className="flex-none w-24 text-center">ID</p>
          <p className="flex-1">{t("general.product")}</p>
          <p className="text-center flex-none w-60">
            {t("catergory_item_list.action")}
          </p>
        </div>
      }
      dataSource={products}
      renderItem={(item) => (
        <List.Item>
          <div className="flex justify-center flex-wrap w-full items-center gap-5 hover:bg-neutral-50 px-3">
            <p className="flex-none w-24 font-bold bg-gray-100 md:bg-transparent text-center">
              <span className="md:hidden mr-2">{t("general.product")}</span>#
              {item.ProductId?.toString()}
            </p>
            <div className="flex-1">
              <div>{item.Manufacturer}</div>
              <h4 className="m-0">{item.Name}</h4>
            </div>
            <div className="flex-none flex justify-center flex-wrap flex-row items-center gap-2">
              <Button
                className="flex-initial"
                danger
                onClick={() => handleremoveproduct(item)}
              >
                {t("catergory_item_list.remove_item_from_category")}
              </Button>
            </div>
          </div>
        </List.Item>
      )}
    ></List>
  );
};

export default CategoryItemList;
