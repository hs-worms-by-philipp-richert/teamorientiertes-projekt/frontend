import { useEffect, useState } from "react";
import Search from "antd/es/input/Search";
import { useTranslation } from "react-i18next";
import {
  DatePicker,
  Divider,
  List,
  Pagination,
  Select,
  Skeleton,
  Tag,
} from "antd";
import { Dayjs } from "dayjs";
import { IOrder } from "../../shared/interfaces/IOrder";
import { OrderStatus } from "../../shared/enums/order-status.enum";
import { orderAPI } from "../api/OrderAPI";
import {
  HistoryOutlined,
  CloudDownloadOutlined,
  StopOutlined,
  CheckOutlined,
  ScheduleOutlined,
  FieldTimeOutlined,
  CarryOutFilled,
  CalendarOutlined,
  InboxOutlined,
} from "@ant-design/icons";
import { palette } from "../helper/status-palette";
import { OrderFilter } from "../interfaces/order-filter.inteface";
import { NavigateFunction, useNavigate } from "react-router-dom";
import { dateTimeFormator } from "../IntlFormat";

const OrderOverviewList = () => {
  const { t } = useTranslation();
  const [orders, setOrders] = useState<IOrder[]>([]);
  const [loading, setloading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);
  const [filterparams, setfilterparams] = useState<OrderFilter>({
    searchterm: "",
    sort: "DESC",
    orderDate: "",
    orderStatus: "",
  });

  const [totallength, settotallength] = useState<number>(0);
  const [page, setpage] = useState<number>(1);
  const defaultPageSize = 30;

  const navigate: NavigateFunction = useNavigate();

  const statusIcons = [
    <InboxOutlined />,
    <CheckOutlined />,
    <HistoryOutlined />,
    <CarryOutFilled />,
    <FieldTimeOutlined />,
    <CloudDownloadOutlined />,
    <StopOutlined />,
  ];

  const fetchOrderList = (page: number = 1, take: number = defaultPageSize) => {
    const fetchData = async () => {
      try {
        setloading(true);
        const orders = await orderAPI.getOrders(page, take);
        setOrders(orders.data.list);
        settotallength(orders.data.totalLength);
        setloading(false);
      } catch (error) {
        setloading(false);
        setError(true);
        console.error(error || t("order_overview_list.get_order_list_error"));
      }
    };
    fetchData();
  };

  useEffect(() => {
    fetchOrderList();
  }, []);

  function handleSearchFilter(searchterm: string): void {
    setfilterparams((filters: any) => ({ ...filters, searchterm: searchterm }));
    handleFiltering(1, defaultPageSize, {
      ...filterparams,
      searchterm: searchterm,
    });
  }

  function handleFiltering(
    page: number = 1,
    take: number = defaultPageSize,
    filters: OrderFilter
  ): void {
    const fetchfiltered = async () => {
      try {
        setloading(true);
        const orders = await orderAPI.getOrders(page, take, filters);
        setOrders(orders.data.list);
        setloading(false);
      } catch (error) {
        setloading(false);
        setError(true);
        console.error(error || t("order_overview_list.get_order_list_error"));
      }
    };
    fetchfiltered();
  }

  function handleSelectStatus(status: string) {
    setfilterparams((filters: any) => ({
      ...filters,
      orderStatus: status ?? "",
    }));
    handleFiltering(1, defaultPageSize, {
      ...filterparams,
      orderStatus: status ?? "",
    });
  }

  function handleDeselectStatus() {
    setfilterparams((filters: any) => ({
      ...filters,
      orderStatus: "",
    }));
    handleFiltering(1, defaultPageSize, {
      ...filterparams,
      orderStatus: "",
    });
  }

  function handleRangeFilter(date: Dayjs): void {
    if (date) {
      const chosenDate = date.format("YYYY-MM-DD");
      setfilterparams((filters: any) => ({
        ...filters,
        orderDate: chosenDate,
      }));
      handleFiltering(1, defaultPageSize, {
        ...filterparams,
        orderDate: chosenDate,
      });
    } else {
      setfilterparams((filters: any) => ({
        ...filters,
        orderDate: "",
      }));
      handleFiltering(1, defaultPageSize, {
        ...filterparams,
        orderDate: "",
      });
    }
  }

  function handleChangeStatus(status: number, orderId: any) {
    const fetchData = async () => {
      try {
        await orderAPI.changeStatus(orderId, status);
        fetchOrderList();
      } catch (error) {
        setloading(true);
        setError(true);
        console.error(error || t("error.get_order_error"));
      }
    };
    fetchData();
  }

  function handleOrderNavigation(id: bigint): void {
    navigate(`order/${id}`);
  }

  return (
    <>
      <div className="flex flex-row flex-wrap gap-4">
        <Search
          className="flex-1 w-full"
          placeholder={t("general.search")}
          allowClear
          size="large"
          value={filterparams.searchterm}
          onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
            handleSearchFilter(event.target.value)
          }
        />
        <Select
          className="w-full text-start flex-1"
          size="large"
          allowClear
          placeholder={t("general.filter_by")}
          options={[
            { value: 0, label: t(`general.${OrderStatus[0]}`) },
            { value: 1, label: t(`general.${OrderStatus[1]}`) },
            { value: 2, label: t(`general.${OrderStatus[2]}`) },
            { value: 3, label: t(`general.${OrderStatus[3]}`) },
            { value: 4, label: t(`general.${OrderStatus[4]}`) },
            {
              value: 5,
              label: t(`general.${OrderStatus[5]}`),
            },
          ]}
          onChange={(event: any) => handleSelectStatus(event)}
          onClear={() => handleDeselectStatus()}
        />
        <DatePicker
          size="large"
          className="flex-1"
          placeholder={t("order_overview_list.rent_am_placeholder")}
          format={"DD.MM.YYYY"}
          onChange={(event: any) => handleRangeFilter(event)}
        ></DatePicker>
      </div>
      <Divider></Divider>
      {!loading ? (
        <div className="w-full overflow-auto">
          <List
            itemLayout="vertical"
            dataSource={orders}
            header={
              <div className="hidden w-full lg:grid lg:grid-cols-6 items-center gap-x-5 bg-neutral-50 font-bold text-center">
                <p className="col-span-1 justify-self-center">
                  {t("order_overview_list.table.order_num")}
                </p>
                <p className="col-span-1">
                  {t("order_overview_list.table.user")}
                </p>
                <p className="col-span-1">
                  {t("order_overview_list.table.date")}
                </p>
                <p className="col-span-1">
                  {t("order_overview_list.table.start")}
                </p>
                <p className="col-span-1">
                  {t("order_overview_list.table.stop")}
                </p>
                <p className="col-span-1">
                  {t("order_overview_list.table.status")}
                </p>
                <Divider className="col-span-6 m-0"></Divider>
              </div>
            }
            renderItem={(item, index) => (
              <List.Item>
                <div
                  onClick={() => handleOrderNavigation(item.OrderId!)}
                  className="w-full grid grid-cols-1 md:grid-cols-6 items-center gap-x-5 hover:bg-neutral-50 hover:cursor-pointer text-center"
                >
                  <p className="col-span-1 md:col-span-1 justify-self-center font-[FiraSansBold] font-black bg-gray-100 md:bg-transparent w-full text-center">
                    <span className="md:hidden mr-2">{t("general.order")}</span>
                    #{item.OrderId?.toString()}
                  </p>
                  <div className="col-span-1 md:col-span-1">
                    <p className="hover:cursor-pointer font-bold">
                      {item.User?.Firstname} {item.User?.Lastname}
                    </p>
                  </div>
                  <div className="col-span-1 md:col-span-1 text-blue-500 text-center">
                    <p>{dateTimeFormator(new Date(item.OrderDate))}</p>
                  </div>
                  <div className="col-span-1 md:col-span-1">
                    <p className="flex gap-1 justify-center">
                      <CalendarOutlined />
                      {item.LineItems[0]
                        ? dateTimeFormator(
                            new Date(item.LineItems[0].StartDate)
                          )
                        : ""}
                    </p>
                  </div>
                  <div className="col-span-1 md:col-span-1">
                    <p className="flex gap-1 justify-center">
                      <ScheduleOutlined />
                      {item.LineItems[0]
                        ? dateTimeFormator(new Date(item.LineItems[0].EndDate))
                        : ""}
                    </p>
                  </div>
                  <div className="col-span-1 md:col-span-1 flex flex-col flex-wrap">
                    <Select
                      onClick={(e) => e.stopPropagation()}
                      className="w-full text-start flex-1"
                      size="large"
                      defaultValue={t(
                        `general.${OrderStatus[item.OrderStatus]}`
                      )}
                      options={[
                        {
                          value: 0,
                          label: (
                            <div className="text-center">
                              <Tag
                                color={palette[0]}
                                className="px-8"
                                key={index}
                              >
                                <h4 className="flex gap-1 ">
                                  {statusIcons[0]}
                                  {t(`general.${OrderStatus[0]}`)}
                                </h4>
                              </Tag>
                            </div>
                          ),
                        },
                        {
                          value: 1,
                          label: (
                            <div className="text-center">
                              <Tag
                                color={palette[1]}
                                className="px-8"
                                key={index}
                              >
                                <h4 className="flex gap-1">
                                  {statusIcons[1]}
                                  {t(`general.${OrderStatus[1]}`)}
                                </h4>
                              </Tag>
                            </div>
                          ),
                        },
                        {
                          value: 2,
                          label: (
                            <div className="text-center">
                              <Tag
                                color={palette[2]}
                                className="px-8"
                                key={index}
                              >
                                <h4 className="flex gap-1">
                                  {statusIcons[2]}
                                  {t(`general.${OrderStatus[2]}`)}
                                </h4>
                              </Tag>
                            </div>
                          ),
                        },
                        {
                          value: 3,
                          label: (
                            <div className="text-center">
                              <Tag
                                color={palette[3]}
                                className="px-8"
                                key={index}
                              >
                                <h4 className="flex gap-1">
                                  {statusIcons[3]}
                                  {t(`general.${OrderStatus[3]}`)}
                                </h4>
                              </Tag>
                            </div>
                          ),
                        },
                        {
                          value: 4,
                          label: (
                            <div className="text-center">
                              <Tag
                                color={palette[4]}
                                className="px-8"
                                key={index}
                              >
                                <h4 className="flex gap-1">
                                  {statusIcons[4]}
                                  {t(`general.${OrderStatus[4]}`)}
                                </h4>
                              </Tag>
                            </div>
                          ),
                        },
                        {
                          value: 5,
                          label: (
                            <div className="text-center">
                              <Tag
                                color={palette[5]}
                                className="px-8"
                                key={index}
                              >
                                <h4 className="flex gap-1">
                                  {statusIcons[5]}
                                  {t(`general.${OrderStatus[5]}`)}
                                </h4>
                              </Tag>
                            </div>
                          ),
                        },
                      ]}
                      onChange={(event: any) =>
                        handleChangeStatus(event, item.OrderId)
                      }
                      onDeselect={() => handleDeselectStatus()}
                    />
                  </div>
                </div>
              </List.Item>
            )}
          ></List>
        </div>
      ) : !error ? (
        <Skeleton paragraph={{ rows: 7 }}></Skeleton>
      ) : (
        <Skeleton paragraph={{ rows: 7 }}></Skeleton>
      )}

      {!loading && orders && orders.length > 0 && (
        <div className="flex justify-center mt-8">
          <Pagination
            onChange={(page, pageSize) => {
              handleFiltering(page, pageSize, filterparams);
              setpage(page);
            }}
            current={page}
            total={totallength}
            pageSize={defaultPageSize}
          />
        </div>
      )}
    </>
  );
};

export default OrderOverviewList;
