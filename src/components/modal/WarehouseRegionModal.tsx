import { Divider, Form, Input, Select } from "antd";
import { useTranslation } from "react-i18next";
import { warehouseAPI } from "../../api/WarehouseAPI";
import { IWarehouse } from "../../../shared/interfaces/IWarehouse";
import { useEffect, useState } from "react";
import { IWarehouseRegion } from "../../../shared/interfaces/IWarehouseRegion";

const WarehouseRegionModal: React.FC<{
  onSubmit: (formdata: IWarehouseRegion) => Promise<void>;
}> = ({ onSubmit }) => {
  const { t } = useTranslation();
  const [warehouselist, setWarehouseList] = useState<IWarehouse[]>([]);

  const getWarehouses = async (page: number = 1, take: number = 30) => {
    try {
      const response = await warehouseAPI.fetchWarehouses(page, take);
      setWarehouseList(response.data.list);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getWarehouses();
  }, []);

  return (
    <div className="w-full">
      <h1 className="text-center">
        {t("storage_region_overview_list.add_region")}
      </h1>
      <Divider></Divider>
      <Form layout="vertical" onFinish={onSubmit} id="regionForm">
        <Form.Item
          name="WarehouseId"
          label={t("storage_region_overview_list.associated_location")}
        >
          <Select
            options={warehouselist.map((warehouse) => ({
              label: warehouse.Name,
              value: warehouse.WarehouseId,
            }))}
          ></Select>
        </Form.Item>
        <Form.Item name="Name" label="Name:">
          <Input></Input>
        </Form.Item>
      </Form>
    </div>
  );
};

export default WarehouseRegionModal;
