import React, { useState, useEffect } from "react";

import { Divider, Form, Input, Select } from "antd";
import { IRole } from "../../../shared/interfaces/IRole";
import { useTranslation } from "react-i18next";
import { IUser } from "../../../shared/interfaces/IUser";
import { userAPI } from "../../api/UserAPIs";

const AddUser: React.FC<{
  handleSubmit: (formdata: IUser) => void;
  handleEdit: (formdata: IUser) => void;
  selecteduser: IUser | undefined;
}> = ({ handleSubmit, selecteduser, handleEdit }) => {
  const { t } = useTranslation();
  const [roles, setRoles] = useState<IRole[]>([]);
  const [form] = Form.useForm();

  useEffect(() => {
    const fetchRoles = async () => {
      try {
        const response = await userAPI.fetchUserRoles();
        setRoles(response.data);
      } catch (error) {
        console.error(t("error.error_role"), error);
      }
    };

    fetchRoles();
  }, []);

  return (
    <>
      <h1 className="text-center">
        {selecteduser ? t("userForm.title_edit") : t("userForm.title_add")}
      </h1>
      <Divider className="mt-0"></Divider>
      <Form
        form={form}
        preserve={false}
        id="userForm"
        layout="vertical"
        onFinish={selecteduser ? handleEdit : handleSubmit}
        initialValues={{...selecteduser, UserId: selecteduser?.UserId?.replace(/-/g, '\/')}}
      >
        <div className="md:grid grid-cols-2 gap-5">
          <div>
            <Divider orientation="left">{t("userForm.user_info")}</Divider>
            <div className="flex gap-3">
              <Form.Item
                label={t("userForm.name1")}
                name="Firstname"
                className="flex-1"
                rules={[
                  {
                    required: true,
                    message: t("message.must_field", {
                      prop: t("userForm.name1"),
                    }),
                  },
                ]}
              >
                <Input placeholder={t("userForm.name1_placeholder")} />
              </Form.Item>
              <Form.Item
                label={t("userForm.name2")}
                className="flex-1"
                name="Lastname"
                rules={[
                  {
                    required: true,
                    message: t("message.must_field", {
                      prop: t("userForm.name2"),
                    }),
                  },
                ]}
              >
                <Input placeholder={t("userForm.name2_placeholder")} />
              </Form.Item>
            </div>
            <Form.Item
              label={t("userForm.member_nr")}
              name="UserId"
              rules={[
                {
                  required: true,
                  pattern: /289\/00\/[0-9]{6}/,
                  message: t("userForm.validation_member_id", {
                    prop: t("userForm.member_nr"),
                  }),
                },
              ]}
            >
              <Input placeholder="289/00/XXXXXX" />
            </Form.Item>
            <div className="flex gap-3">
              <Form.Item
                label={t("userForm.mail")}
                name="Email"
                className="flex-1"
                rules={[
                  {
                    required: true,
                    pattern: /^[\w\-\.]+@([\w-]+\.)+[\w-]{2,}$/,
                    message: t("userForm.validation_email"),
                  },
                ]}
              >
                <Input placeholder={t("userForm.mail")} />
              </Form.Item>
              <Form.Item
                label={t("userForm.phone")}
                name="Phone"
                className="flex-1"
                rules={[
                  {
                    required: true,
                    message: t("message.must_field"),
                  },
                ]}
              >
                <Input placeholder={t("userForm.phone_placeholder")} />
              </Form.Item>
            </div>
            <Form.Item
              label={t("general.password")}
              name="Password"
              rules={[
                {
                  required: !selecteduser,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input.Password
                placeholder={
                  selecteduser
                    ? t("userForm.update_password")
                    : t("general.password")
                }
              />
            </Form.Item>
            <Form.Item
              label="Rolle"
              name="RoleId"
              initialValue={t("userForm.rol_choice")}
            >
              <Select>
                {roles.map((role: IRole) => (
                  <Select.Option key={role.Name} value={role.RoleId}>
                    {role.Name}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </div>
          <div>
            <Divider orientation="left">{t("userForm.personal_info")}</Divider>
            <div className="flex gap-3">
              <Form.Item
                label={t("userForm.street")}
                name="Street"
                className="flex-1"
                rules={[
                  {
                    required: true,
                    message: t("message.must_field"),
                  },
                ]}
              >
                <Input placeholder={t("userForm.street")} />
              </Form.Item>
              <Form.Item
                label={t("userForm.house_number")}
                name="Housenumber"
                rules={[
                  {
                    max: 4,
                    required: true,
                    message: t("userForm.validation_housenumber"),
                  },
                ]}
              >
                <Input placeholder={t("userForm.house_number")} />
              </Form.Item>
            </div>
            <Form.Item
              label={t("userForm.city")}
              name="City"
              rules={[
                {
                  required: true,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input placeholder={t("userForm.city")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.zip_code")}
              name="Postal"
              rules={[
                {
                  min: 5,
                  max: 5,
                  required: true,
                  message: t("userForm.validation_postal"),
                },
              ]}
            >
              <Input placeholder={t("userForm.zip_code_placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.country")}
              name="Country"
              rules={[
                {
                  required: true,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input placeholder={t("userForm.country")} />
            </Form.Item>
          </div>

          {/* <Form.Item label="">
            <Button className="ml-24" type="primary" htmlType="submit">
              {t("userForm.confirm_button")}
            </Button>
          </Form.Item> */}
        </div>
      </Form>
    </>
  );
};

export default AddUser;
