import React, { BaseSyntheticEvent, useEffect, useState } from "react";
import {
  Form,
  Input,
  Radio,
  Divider,
  InputNumber,
  message,
  Button,
  Popconfirm,
  TreeSelect,
  Upload,
  Tag,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { IProduct } from "../../../shared/interfaces/IProduct";
import { ICategory } from "../../../shared/interfaces/ICategory";
import {
  DeleteOutlined,
  FilePdfOutlined,
  FileImageOutlined,
} from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import { categoryAPI } from "../../api/CategoryAPI";
import { productAPI } from "../../api/ProductAPI";
import { ProductForm } from "../../interfaces/product-form.interface";
import { IProductImage } from "../../../shared/interfaces/IProductImage";
import { UploadFile } from "antd/lib";

interface CustomFile extends File {
  uid: string;
  id: string; // Ajout de l'identifiant temporaire
}

interface Props {
  selecteditem: IProduct | undefined | null;
  updateproducts: () => void;
}

interface CategoryTree {
  value: bigint;
  label: string;
  children: { value: bigint; label: string }[];
}

const InventoryModal: React.FC<Props> = ({ selecteditem, updateproducts }) => {
  const { t } = useTranslation();
  const [imageFileList, setImageFileList] = useState<CustomFile[]>([]);
  const [manualFileList, setManualFileList] = useState<CustomFile[]>([]);
  const [categories, setCategories] = useState<ICategory[]>([]);
  const [images, setImages] = useState<UploadFile[] | undefined>([]);
  const [messageAPI, contextHolder] = message.useMessage();
  const [documents, setDocuments] = useState<
    { name: string; url: string; id: string }[] | undefined
  >([]);

  const generateId = () => {
    return Math.random().toString(36).substr(2, 9); // generate a unique temporaqry identifier
  };

  const handleImageUploadChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const files = event.target.files;
    if (files) {
      const fileList = Array.from(files) as CustomFile[];
      fileList.forEach((file) => {
        file.id = generateId(); // gives a unique temporaqry identifier to each file
      });
      setImageFileList([...imageFileList, ...fileList]);
      messageAPI.success(
        `${fileList.length} ${t("inventory_modal.image_select_success")}`
      );
    }
  };

  const handleManualUploadChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const files = event.target.files;
    if (files) {
      const allowedExtensions = [".pdf"];
      const fileList = Array.from(files) as CustomFile[];
      fileList.forEach((file) => {
        file.id = generateId(); // gives a unique temporaqry identifier to each file
      });
      const isValidFiles = fileList.every((file) => {
        const fileExtension = file.name
          .slice(file.name.lastIndexOf("."))
          .toLowerCase();
        return allowedExtensions.includes(fileExtension);
      });
      if (isValidFiles) {
        setManualFileList([...manualFileList, ...fileList]);
        messageAPI.success(
          `${fileList.length} ${t("inventory_modal.pdf_select_success")}`
        );
      } else {
        messageAPI.error(t("inventory_modal.pdf_select_error"));
      }
    }
  };

  useEffect(() => {
    const getCategories = async () => {
      try {
        const categories = await categoryAPI.fetchCategories();
        if (selecteditem) {
          const product = await productAPI.fetchProduct(
            selecteditem?.ProductId
          );
          getFiles(product.data);
        }
        setCategories(categories.data.list);
      } catch (error) {
        console.error(error || t("inventory_modal.get_categories_error"));
      }
    };
    getCategories();
  }, []);

  const getFiles = (product: IProduct) => {
    const images = product?.ProductImages?.map(
      (image: IProductImage, index) => ({
        uid: image.ProductId.toString(),
        name: `Bild${index}`,
        fileName: image.ImageId,
        url: `${import.meta.env.VITE_REACT_API_URL}/file/product-image/${
          image.ProductId
        }/${image.ImageId}`,
      })
    );
    const documents = product?.ProductManuals?.map((document) => ({
      name: document.Name,
      id: document.ManualId,
      url: `${import.meta.env.VITE_REACT_API_URL}/file/product-image/${
        document.ProductId
      }/${document.ManualId}`,
    }));
    setImages(images);
    setDocuments(documents);
  };

  const getTreeData = (categories: ICategory[]) => {
    const treedata: CategoryTree[] = [];
    categories.forEach((category: ICategory) =>
      category.ParentCategoryId === null
        ? treedata.push({
            value: category.CategoryId as bigint,
            label: category.Name,
            children: [],
          })
        : {}
    );
    treedata.forEach((category) =>
      categories.forEach((childcategory: ICategory) => {
        childcategory.ParentCategoryId?.CategoryId === category.value
          ? category.children.push({
              value: childcategory.CategoryId as bigint,
              label: childcategory.Name,
            })
          : {};
      })
    );
    return treedata;
  };

  const onFinish = async (values: ProductForm) => {
    try {
      if (!selecteditem) {
        const productId = await productAPI.postProduct(values as any);
        const paredProductId = BigInt(productId);
        values.Categories.forEach(async (categoryid: bigint) => {
          await categoryAPI.createProductCategory(paredProductId, categoryid);
        });
        await productAPI.postProductImage(paredProductId, imageFileList);
        await productAPI.postProductManual(paredProductId, manualFileList);
      } else {
        await productAPI.updateProduct({
          ...values,
          ProductId: selecteditem.ProductId,
          Categories: undefined,
        });
        if (
          !selecteditem?.Categories?.some((category) =>
            values.Categories.includes(BigInt(category.CategoryId))
          )
        ) {
          await categoryAPI.deleteProductCategoriesFromProduct(
            selecteditem.ProductId as bigint
          );
          values.Categories.forEach(async (categoryid) => {
            await categoryAPI.createProductCategory(
              selecteditem.ProductId as bigint,
              categoryid
            );
          });
        }
        await productAPI.postProductImage(
          selecteditem.ProductId!,
          imageFileList
        );
        await productAPI.postProductManual(
          selecteditem.ProductId!,
          manualFileList
        );
      }

      messageAPI.success(t("inventory_modal.upload_success"));
      updateproducts();
    } catch (error) {
      console.error(`${t("general.error")}:`, error);
      messageAPI.error(`${t("general.error")}.`);
    }
  };

  const handleProductData = () => {
    const data = {
      ...selecteditem,
      Categories: selecteditem?.Categories?.map(
        (category) => category?.CategoryId
      ),
    };

    return data;
  };

  const removeImage = async (id: string) => {
    const updatedList = imageFileList.filter((item) => item.id !== id);
    setImageFileList(updatedList);
    updateproducts();
  };

  const removeManual = async (id: string) => {
    const updatedList = manualFileList.filter((item) => item.id !== id);
    setManualFileList(updatedList);
    updateproducts();
  };

  const deleteImage = async (e: UploadFile) => {
    try {
      await productAPI.deleteProductImage(BigInt(e.uid), e.fileName as string);
      messageAPI.success(t("inventory_modal.delete_image"));
      setImages((images) =>
        images?.filter((image) => image.fileName != e.fileName)
      );
    } catch (error) {
      console.error(error);
      messageAPI.error(t("inventory_modal.delete_image_error"));
    }
  };

  const deleteDocument = async (event: BaseSyntheticEvent, id: string) => {
    event.preventDefault();

    try {
      if (selecteditem) {
        await productAPI.deleteProductManual(
          selecteditem?.ProductId as bigint,
          id
        );
        setDocuments((documents) =>
          documents?.filter((document) => document.id != id)
        );
        messageAPI.success("inventory_modal.delete_image_error");
      }
    } catch (error) {
      console.error();
    }
  };

  // const images = [
  //   {
  //     uid: "-1",
  //     name: "image.png",
  //     status: "done",
  //     url: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
  //   },
  // ];

  return (
    <div className="w-full">
      {contextHolder}
      <h1 className="text-center">{t("inventory_modal.add_items")}</h1>
      <Divider></Divider>
      <Form
        className="p-4 mt-4"
        id="inventoryForm"
        layout="vertical"
        onFinish={onFinish}
        initialValues={handleProductData()}
      >
        <div className="md:grid grid-cols-2 gap-5">
          <Form.Item
            name="Manufacturer"
            label={t("general.manufacturer")}
            rules={[{ required: true }]}
          >
            <Input placeholder="Hersteller"></Input>
          </Form.Item>
          <Form.Item
            name="Name"
            label={t("inventory_modal.product_name")}
            rules={[{ required: true }]}
          >
            <Input placeholder={t("inventory_modal.product_name")}></Input>
          </Form.Item>
          <Form.Item
            label={t("inventory_modal.product_description")}
            name="Description"
            rules={[{ required: true }]}
          >
            <TextArea
              placeholder={t("inventory_modal.product_description_placeholder")}
            ></TextArea>
          </Form.Item>
          <Form.Item
            name="RentingFee"
            label={t("inventory_modal.product_renting_fee")}
            rules={[{ required: true }]}
          >
            <InputNumber
              placeholder={t("inventory_modal.product_renting_fee_placeholder")}
              addonAfter={
                <div className="mx-4">
                  {t("inventory_modal.product_renting_fee_placeholder_2")}
                </div>
              }
              className="w-full"
            ></InputNumber>
          </Form.Item>
          <Form.Item
            name="Categories"
            label={t("general.categories")}
            rules={[{ required: true }]}
          >
            <TreeSelect
              multiple
              placeholder={t("inventory_modal.product_categories_placeholder")}
              treeData={getTreeData(categories)}
            ></TreeSelect>
          </Form.Item>
          <Form.Item
            name="IsInternal"
            label={t("inventory_modal.product_accessibility")}
            initialValue={false}
            rules={[{ required: true }]}
          >
            <Radio.Group>
              <Radio value={false}>
                {t("inventory_modal.product_accessibility_option_1")}
              </Radio>
              <Radio value={true}>
                {t("inventory_modal.product_accessibility_option_2")}
              </Radio>
            </Radio.Group>
          </Form.Item>
        </div>
        {images?.length ? (
          <Form.Item label={t("inventory_modal.uploaded_images")}>
            <Upload
              listType="picture-card"
              fileList={images}
              onRemove={(e) => deleteImage(e)}
            ></Upload>
          </Form.Item>
        ) : undefined}
        {documents?.length ? (
          <Form.Item label={t("inventory_modal.uploaded_pdfs")}>
            {documents?.map((document) => (
              <Tag
                closable
                onClose={(e: BaseSyntheticEvent) =>
                  deleteDocument(e, document.id)
                }
                icon={<FilePdfOutlined />}
              >
                {document.name}
              </Tag>
            ))}
          </Form.Item>
        ) : undefined}
        <div className="md:grid grid-cols-2 gap-5">
          <Form.Item label={t("inventory_modal.upload_images")}>
            <label htmlFor="image-upload" className="custom-file-upload">
              <input
                id="image-upload"
                type="file"
                accept=".png,.jpg,.jpeg"
                onChange={handleImageUploadChange}
                multiple
              />
            </label>
            <div className="uploaded-files">
              {imageFileList.map((file) => (
                <div key={file.uid} className="uploaded-file">
                  <span>
                    <FileImageOutlined style={{ color: "#1890ff" }} />
                    {file.name}
                  </span>
                  <Popconfirm
                    title={t("item_inventory_list.delete_confirmation")}
                    onConfirm={() => removeImage(file.id)}
                  >
                    <Button danger type="link" icon={<DeleteOutlined />} />
                  </Popconfirm>
                </div>
              ))}
            </div>
          </Form.Item>
          <Form.Item label={t("inventory_modal.upload_pdfs")}>
            <label htmlFor="manual-upload" className="custom-file-upload">
              <input
                id="manual-upload"
                type="file"
                accept=".pdf"
                onChange={handleManualUploadChange}
                multiple
              />
            </label>
            <div className="uploaded-files">
              {manualFileList.map((file) => (
                <div key={file.uid} className="uploaded-file">
                  <span>
                    <FilePdfOutlined style={{ color: "#f5222d" }} />
                    {file.name}
                  </span>
                  <Popconfirm
                    title={t("item_inventory_list.delete_confirmation")}
                    onConfirm={() => removeManual(file.id)}
                  >
                    <Button danger type="link" icon={<DeleteOutlined />} />
                  </Popconfirm>
                </div>
              ))}
            </div>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
};

export default InventoryModal;
