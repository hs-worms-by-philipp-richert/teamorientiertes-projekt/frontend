import { Divider, Form, Input } from "antd";
import { useTranslation } from "react-i18next";

const WarehouseModal: React.FC<{
  selected: "region" | "storage" | "tray";
  handlesubmit: (formdata: any) => Promise<void>;
}> = ({ selected, handlesubmit }) => {
  const { t } = useTranslation();

  return (
    <>
      <h1 className="text-center">
        {t(`storage_region_overview_list.add_${selected}`)}
      </h1>
      <Divider></Divider>
      <Form id="warehouseform" layout="vertical" onFinish={handlesubmit}>
        <Form.Item name="Name" label="Name" rules={[{ required: true }]}>
          <Input placeholder="Name"></Input>
        </Form.Item>
      </Form>
    </>
  );
};

export default WarehouseModal;
