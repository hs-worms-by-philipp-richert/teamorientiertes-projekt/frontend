import { Button, Card, Divider, Form, message } from "antd";
import { useState } from "react";
import { FilePdfOutlined, DeleteOutlined } from "@ant-design/icons";
import createPsaCheck from "../../api/createPsaCheck";
import { useTranslation } from "react-i18next";

interface PsaFile extends File {
  itemId: bigint;
  psaId?: string;
}

const PsaAddModal: React.FC<{ itemId: bigint; updateList: () => void }> = ({
  itemId,
  updateList,
}) => {
  const { t } = useTranslation();

  const [psaFileState, setPsaFileState] = useState<PsaFile>();
  const [messageAPI, contextHolder] = message.useMessage();

  const onFinish = async () => {
    if (!psaFileState) return;

    try {
      await createPsaCheck(itemId, psaFileState);
      messageAPI.success(t("psa_check.upload_modal.upload_successful"));
      updateList();
    } catch (error) {
      console.error(error);
      messageAPI.error(t("psa_check.upload_modal.upload_error"));
    }
  };

  const handlePsaUploadChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const files = event.target.files;

    if (files) {
      const allowedExtensions = [".pdf"];
      const file = files[0] as PsaFile;
      file.itemId = itemId;

      const fileExtension = file.name
        .slice(file.name.lastIndexOf("."))
        .toLowerCase();
      const isValidFile = allowedExtensions.includes(fileExtension);

      if (isValidFile) {
        setPsaFileState(file);
      } else {
        messageAPI.error(t("psa_check.upload_modal.invalid_file_format"));
      }
    }
  };

  return (
    <>
      {contextHolder}
      <div className="w-full">
        <h1 className="text-center">{t("psa_check.upload_modal.title")}</h1>
        <Divider></Divider>
        <Form
          className="p-4 mt-4"
          id="psaForm"
          layout="vertical"
          onFinish={onFinish}
        >
          <div className="md:grid grid-cols-2 gap-5">
            <Form.Item label={t("psa_check.upload_modal.label_text")}>
              <Card style={{ maxWidth: 500 }}>
                <label htmlFor="pdf-upload">
                  <input
                    id="pdf-upload"
                    type="file"
                    accept=".pdf"
                    onChange={handlePsaUploadChange}
                  ></input>
                  <div className="uploaded-files">
                    {psaFileState ? (
                      <div className="uploaded-file mt-4">
                        <span>
                          <FilePdfOutlined
                            className="mr-2"
                            style={{ color: "#f5222d" }}
                          />
                          {psaFileState?.name}
                        </span>

                        <Button
                          danger
                          className="ml-4"
                          icon={<DeleteOutlined />}
                          onClick={() => setPsaFileState(undefined)}
                        />
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                </label>
              </Card>
            </Form.Item>
          </div>
        </Form>
      </div>
    </>
  );
};

export default PsaAddModal;
