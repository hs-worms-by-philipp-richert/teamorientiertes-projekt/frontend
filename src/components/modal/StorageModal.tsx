import { Divider, Form, Input, InputNumber } from "antd";
import { useTranslation } from "react-i18next";
import { IWarehouse } from "../../../shared/interfaces/IWarehouse";

const StorageModal = ({
  handlesubmit,
  selected,
}: {
  handlesubmit: Function;
  selected: IWarehouse | undefined;
}) => {
  const { t } = useTranslation();

  return (
    <div className="w-full">
      <h1 className="text-center">{t("storage_modal.add_warehouse")}</h1>
      <Divider></Divider>
      <Form
        preserve={false}
        className="p-4 mt-4"
        layout="vertical"
        id="warehouseForm"
        initialValues={selected}
        onFinish={(fieldsvalue) => handlesubmit(fieldsvalue)}
      >
        <Form.Item
          label={t("storage_modal.warehouse_name")}
          name="Name"
          rules={[{ required: true }]}
        >
          <Input placeholder={t("storage_modal.warehouse_name")}></Input>
        </Form.Item>
        <div className="flex gap-3">
          <Form.Item
            label={t("storage_modal.street")}
            name="Street"
            className="flex-1"
            rules={[{ required: true }]}
          >
            <Input placeholder={t("storage_modal.street")}></Input>
          </Form.Item>
          <Form.Item
            className="flex-none"
            label={t("storage_modal.house_number")}
            name="Housenumber"
            rules={[{ required: true }]}
          >
            <InputNumber
              className="w-full"
              placeholder={t("storage_modal.house_number")}
            ></InputNumber>
          </Form.Item>
        </div>
        <div className="flex gap-3">
          <Form.Item
            label={t("storage_modal.postal")}
            name="Postal"
            rules={[{ required: true }]}
          >
            <InputNumber
              className="w-full"
              placeholder={t("storage_modal.postal")}
            ></InputNumber>
          </Form.Item>
          <Form.Item
            label={t("storage_modal.city")}
            name="City"
            className="flex-1"
            rules={[{ required: true }]}
          >
            <Input placeholder={t("storage_modal.city")}></Input>
          </Form.Item>
        </div>
        <Form.Item
          label={t("storage_modal.country")}
          name="Country"
          rules={[{ required: true }]}
        >
          <Input placeholder={t("storage_modal.country")}></Input>
        </Form.Item>
      </Form>
    </div>
  );
};

export default StorageModal;
