import React, { useState, useEffect } from "react";

import { Divider, Form, Input } from "antd";
import { useTranslation } from "react-i18next";
import { IUser } from "../../../shared/interfaces/IUser";
import { getUserData } from "../../util/auth";

const EditUserForm: React.FC <{ updateUser: (newUserData: IUser) => void; }> = ({updateUser}) => {
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const [userData, setUserData] = useState<IUser>();

  const fetchUser = async () => {
    try {
      const user: IUser = await getUserData();
      setUserData(user);
      form.setFieldsValue({
        Firstname: user.Firstname,
        Lastname: user.Lastname,
        Email: user.Email,
        Phone: user.Phone,
        Street: user.Street,
        Housenumber: user.Housenumber,
        Postal: user.Postal,
        City: user.City,
        Country: user.Country
      });
    } catch (error) {
      console.error(error || t("general.get_categories_error"));
    }
  };

  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <>
      <h1 className="text-center">
       {t("userForm.title_edit")}
      </h1>
      <Divider className="mt-0"></Divider>

      <Form form={form} id="userEditForm" onFinish={updateUser} initialValues={userData} layout="vertical">
        <div className="md:grid grid-cols-2 gap-5">
          <div>
            <Divider orientation="left">{t("userForm.user_info")}</Divider>
            <Form.Item
              label={t("userForm.name1")}
              name="Firstname"
              className="flex-1"
              rules={[
                {
                  required: true,
                  message: t("message.must_field", {
                    prop: t("userForm.name1"),
                  }),
                },
              ]}
            >
              <Input placeholder={t("userForm.name1_placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.name2")}
              className="flex-1"
              name="Lastname"
              rules={[
                {
                  required: true,
                  message: t("message.must_field", {
                    prop: t("userForm.name2"),
                  }),
                },
              ]}
            >
              <Input placeholder={t("userForm.name2_placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.mail")}
              name="Email"
              className="flex-1"
              rules={[
                {
                  required: true,
                  pattern: /^[\w\-\.]+@([\w-]+\.)+[\w-]{2,}$/,
                  message: t("userForm.validation_email"),
                },
              ]}
            >
              <Input placeholder={t("userForm.mail")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.phone")}
              name="Phone"
              className="flex-1"
              rules={[
                {
                  required: true,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input placeholder={t("userForm.phone_placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("general.password")}
              name="Password"
              rules={[
                {
                  required: false,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input.Password
                placeholder={t("userForm.update_password")}
              />
            </Form.Item>
          </div>
          <div>
            <Divider orientation="left">{t("userForm.personal_info")}</Divider>
            <Form.Item
              label={t("userForm.street")}
              name="Street"
              className="flex-1"
              rules={[
                {
                  required: true,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input placeholder={t("userForm.street")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.house_number")}
              name="Housenumber"
              rules={[
                {
                  max: 4,
                  required: true,
                  message: t("userForm.validation_housenumber"),
                },
              ]}
            >
              <Input placeholder={t("userForm.house_number")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.city")}
              name="City"
              rules={[
                {
                  required: true,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input placeholder={t("userForm.city")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.zip_code")}
              name="Postal"
              rules={[
                {
                  min: 5,
                  max: 5,
                  required: true,
                  message: t("userForm.validation_postal"),
                },
              ]}
            >
              <Input placeholder={t("userForm.zip_code_placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("userForm.country")}
              name="Country"
              rules={[
                {
                  required: true,
                  message: t("message.must_field"),
                },
              ]}
            >
              <Input placeholder={t("userForm.country")} />
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default EditUserForm;
