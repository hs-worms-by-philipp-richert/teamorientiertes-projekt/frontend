import { useTranslation } from "react-i18next";
import { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Divider,
  List,
  Modal,
  Popconfirm,
  Skeleton,
  message,
} from "antd";
import {
  PlusOutlined,
  DownloadOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { IProductItem } from "../../shared/interfaces/IProductItem";
import PsaAddModal from "./modal/PsaAddModal";
import { psaAPI } from "../api/PsaAPI";
import { fileAPI } from "../api/FileAPI";

const PsaCheckItem = ({
  modalopen,
  setmodalopen,
  itemid,
  setname,
  setstorage,
}: {
  modalopen: { showPsaModal: boolean };
  setmodalopen: Function;
  itemid: bigint;
  setname: Function;
  setstorage: Function;
}) => {
  const { t } = useTranslation();

  const [psalist, setpsalist] = useState<IProductItem | undefined>();
  const [loading, setloading] = useState<boolean>(false);
  const [messageAPI, contextHolder] = message.useMessage();

  const closeModal = () => {
    setmodalopen(false);
  };

  const fetchPsaList = () => {
    const fetchData = async () => {
      try {
        closeModal();
        setloading(true);
        const result = await psaAPI.fetchPsaListByItem(itemid);
        setpsalist(result.data);
        setname(result.data.Product.Name, result.data.QrCode);
        setstorage(result.data.Tray);
        setloading(false);
      } catch (error) {
        setloading(false);
        console.error(error);
      }
    };

    fetchData();
  };

  const deletePsa = async (itemId: bigint, psaId: string) => {
    try {
      setloading(true);
      await psaAPI.deletePsaFile(itemId, psaId);
      fetchPsaList();
      setloading(false);
    } catch (error) {
      console.error(error);
      messageAPI.error(t("error.error_delete_psa"));
    }
  };

  useEffect(() => {
    fetchPsaList();
  }, []);

  const confirm = async (itemId: bigint, psaId: string) => {
    await deletePsa(itemId, psaId);
    messageAPI.success("PDF deleted");
  };

  return (
    <>
      {contextHolder}
      {!loading && psalist ? (
        <List
          header={
            <div className="w-full grid md:grid-cols-3 items-center gap-x-5 bg-neutral-50 font-bold">
              <p className="col-span-2 justify-self-center">
                {t("psa_check.item_check.check_date")}
              </p>
              <p className="text-center">
                {t("psa_check.item_check.list_actions")}
              </p>
              <Divider className="col-span-3 m-0"></Divider>
            </div>
          }
          dataSource={psalist?.PSA}
          renderItem={(item, index) => (
            <List.Item key={index}>
              <div className="w-full grid md:grid-cols-3 items-center justify-center gap-5 hover:bg-neutral-50 px-3">
                <div className="col-span-2 justify-self-center">
                  <div className="flex">
                    {new Date(item.DateChecked).toLocaleString()}
                  </div>
                </div>
                <div className="flex justify-center flex-wrap col-span-1 flex-row items-center gap-2">
                  <div className="flex">
                    <Button
                      className="mr-4"
                      icon={<DownloadOutlined />}
                      target="_blank"
                      href={fileAPI.getPSACheck(item.ItemId, item.PsaId)}
                    >
                      {t("psa_check.item_check.action_show_pdf")}
                    </Button>

                    <Popconfirm
                      title="Delete Item-Check"
                      description={t("psa_check.item_check.confirm_delete_pdf")}
                      onConfirm={() => confirm(item.ItemId, item.PsaId)}
                      okText={t("buttons.delete")}
                      okType="danger"
                      cancelText={t("buttons.cancel")}
                    >
                      <Button
                        danger
                        type="link"
                        className="flex-1"
                        icon={<DeleteOutlined />}
                      >
                        {t("buttons.delete")}
                      </Button>
                    </Popconfirm>
                  </div>
                </div>
              </div>
            </List.Item>
          )}
        ></List>
      ) : !loading && !psalist ? (
        <Alert
          message={t("psa_check.item_check.item_not_exist")}
          className="mt-4"
          type="warning"
        />
      ) : (
        <List.Item>
          <div className="w-full">
            <Skeleton></Skeleton>
            <Skeleton></Skeleton>
          </div>
        </List.Item>
      )}

      <Modal
        centered
        destroyOnClose
        open={modalopen.showPsaModal}
        onCancel={closeModal}
        width={"75%"}
        okText={t("buttons.add")}
        cancelText={t("buttons.cancel")}
        okButtonProps={{
          type: "primary",
          form: "psaForm",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
      >
        <PsaAddModal updateList={fetchPsaList} itemId={itemid}></PsaAddModal>
      </Modal>
    </>
  );
};

export default PsaCheckItem;
