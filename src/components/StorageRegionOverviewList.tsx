import { Button, Modal, Popconfirm, Table, message } from "antd";
import { IWarehouseRegion } from "../../shared/interfaces/IWarehouseRegion";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import { warehouseAPI } from "../api/WarehouseAPI";
import { useEffect, useState } from "react";
import WarehouseModal from "./modal/WarehouseModal";
import { IWarehouseStorage } from "../../shared/interfaces/IWarehouseStorage";
import { IWarehouseStorageTray } from "../../shared/interfaces/IWarehouseStorageTray";

const StorageRegionOverviewList: React.FC<{ loading: boolean }> = ({
  loading,
}) => {
  const { t } = useTranslation();
  const [regions, setRegions] = useState<IWarehouseRegion[]>([]);
  const [modalopen, setModalOpen] = useState<boolean>(false);
  const [messageAPI, contextHolder] = message.useMessage();
  const [selectedStorageType, setSelectedStorageType] = useState<
    "region" | "storage" | "tray"
  >("region");
  const [selectedId, setSelectedId] = useState<bigint>();

  const getRegions = async (page: number = 1, take: number = 30) => {
    try {
      const regions = await warehouseAPI.fetchRegions(page, take);
      setRegions(regions.data.list);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getRegions();
  }, [modalopen, loading]);

  const openModal = (
    selectedType: "region" | "storage" | "tray",
    id: bigint
  ) => {
    setModalOpen(true);
    setSelectedStorageType(selectedType);
    setSelectedId(id);
  };

  // const handleAddRegion = async (formdata: any) => {
  //   try {
  //     await warehouseAPI.createRegion({ ...formdata, WarehouseId: selectedId });
  //     setModalOpen(false);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  const handleAddStorage = async (formdata: any) => {
    try {
      await warehouseAPI.createStorage({ ...formdata, RegionId: selectedId });
      setModalOpen(false);
      messageAPI.success(t("storage_region_overview_list.storage_success"));
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddTray = async (formdata: any) => {
    try {
      await warehouseAPI.createTray({ ...formdata, StorageId: selectedId });
      setModalOpen(false);
      messageAPI.success(t("storage_region_overview_list.tray_success"));
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteRegion = async (regionid: bigint) => {
    try {
      await warehouseAPI.deleteRegion(regionid);
      getRegions();
      messageAPI.success(t("storage_region_overview_list.delete_storage"));
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteStorage = async (storageid: bigint) => {
    try {
      await warehouseAPI.deleteStorage(storageid);
      getRegions();
      messageAPI.success(t("storage_region_overview_list.delete_storage"));
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteTray = async (trayid: bigint) => {
    try {
      await warehouseAPI.deleteTrayById(trayid);
      getRegions();
      messageAPI.success(t("storage_region_overview_list.delete_storage"));
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      {contextHolder}
      <Table
        scroll={{ x: 500 }}
        bordered
        pagination={false}
        dataSource={regions.map((regions) => ({
          ...regions,
          key: regions.RegionId,
        }))}
        expandable={{
          rowExpandable: (record) => record.Storages?.length != 0,
          defaultExpandAllRows: false,
          expandRowByClick: true,
          expandedRowRender: (record) => (
            <>
              <Table
                bordered
                pagination={false}
                dataSource={record.Storages?.map((storage) => ({
                  ...storage,
                  key: storage.StorageId,
                }))}
                columns={[
                  {
                    title: t("storage_region_overview_list.storage"),
                    key: "storagename",
                    dataIndex: "Name",
                    width: "60vw",
                  },
                  {
                    title: t("general.actions"),
                    key: "actions_storage",
                    render: (storage: IWarehouseStorage) => (
                      <div className="flex flex-wrap gap-5 items-center justify-center">
                        <Popconfirm
                          title={t("general.delete") + "?"}
                          okText={t("general.delete")}
                          onPopupClick={(e) => e.stopPropagation()}
                          okButtonProps={{ type: "primary" }}
                          okType="danger"
                          onConfirm={() => {
                            handleDeleteStorage(storage.StorageId as bigint);
                          }}
                        >
                          <Button
                            className="flex-1"
                            danger
                            icon={<DeleteOutlined />}
                            type="link"
                            onClick={(e) => {
                              e.stopPropagation();
                            }}
                          >
                            {t("general.delete")}
                          </Button>
                        </Popconfirm>

                        <Button
                          icon={<PlusOutlined />}
                          className="flex-1"
                          onClick={(e) => {
                            e.stopPropagation();
                            openModal("tray", storage.StorageId as bigint);
                          }}
                        >
                          Regal {t("general.add")}
                        </Button>
                      </div>
                    ),
                  },
                ]}
                expandable={{
                  defaultExpandAllRows: false,
                  expandRowByClick: true,
                  rowExpandable: (record) => record.Trays?.length != 0,
                  expandedRowRender: (record) => (
                    <>
                      <Table
                        bordered
                        pagination={false}
                        expandable={{
                          expandedRowRender: () => <></>,
                          expandIcon: () => <></>,
                        }}
                        dataSource={record.Trays?.map((tray) => ({
                          ...tray,
                          key: tray.TrayId,
                        }))}
                        columns={[
                          {
                            title: t("storage_region_overview_list.tray"),
                            key: "trayname",
                            dataIndex: "Name",
                            width: "60vw",
                          },
                          {
                            title: t("general.actions"),
                            key: "actions_trays",
                            render: (tray: IWarehouseStorageTray) => (
                              <div className="flex flex-wrap gap-5 items-center justify-center">
                                <Popconfirm
                                  title={t("general.delete") + "?"}
                                  okText={t("general.delete")}
                                  okButtonProps={{ type: "primary" }}
                                  okType="danger"
                                  onConfirm={() => {
                                    handleDeleteTray(tray.TrayId as bigint);
                                  }}
                                >
                                  <Button
                                    type="link"
                                    className="flex-1"
                                    danger
                                    icon={<DeleteOutlined />}
                                  >
                                    Regal {t("general.delete")}
                                  </Button>
                                </Popconfirm>
                              </div>
                            ),
                          },
                        ]}
                      ></Table>
                    </>
                  ),
                }}
              ></Table>
            </>
          ),
        }}
        columns={[
          {
            title: t("storage_region_overview_list.region"),
            dataIndex: "Name",
            width: "30vw",
            key: "name",
          },
          {
            title: t("storage_region_overview_list.associated_location"),
            key: "warehouse",
            render: (region) => <>{region.Warehouse.Name}</>,
          },
          {
            title: t("general.actions"),
            dataIndex: "",
            key: "actions_regions",
            render: (region: IWarehouseRegion) => (
              <>
                <div className="flex flex-wrap gap-5 items-center justify-center">
                  <Popconfirm
                    title={t("general.delete") + "?"}
                    okText={t("general.delete")}
                    onPopupClick={(e) => e.stopPropagation()}
                    okButtonProps={{ type: "primary" }}
                    okType="danger"
                    onConfirm={() => {
                      handleDeleteRegion(region.RegionId as bigint);
                    }}
                  >
                    <Button
                      className="flex-1"
                      danger
                      type="link"
                      onClick={(e) => e.stopPropagation()}
                      icon={<DeleteOutlined />}
                    >
                      {t("general.delete")}
                    </Button>
                  </Popconfirm>
                  <Button
                    className="flex-1"
                    icon={<PlusOutlined />}
                    onClick={(e) => {
                      e.stopPropagation();
                      openModal("storage", region.RegionId as bigint);
                    }}
                  >
                    {t("storage_region_overview_list.add_storage")}
                  </Button>
                </div>
              </>
            ),
          },
        ]}
      ></Table>
      <Modal
        destroyOnClose
        okButtonProps={{
          type: "primary",
          form: "warehouseform",
          htmlType: "submit",
          icon: <PlusOutlined />,
        }}
        open={modalopen}
        onCancel={() => setModalOpen(false)}
      >
        <WarehouseModal
          selected={selectedStorageType}
          handlesubmit={
            selectedStorageType === "storage" ? handleAddStorage : handleAddTray
          }
        ></WarehouseModal>
      </Modal>
    </>
  );
};

export default StorageRegionOverviewList;
