import axiosBase from './axiosBase';

class PsaAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async fetchPsaListByItem(itemId: bigint) {
    const psalist = await axiosBase.get(`${this.BACKENDHOST}/psa/list/${itemId}`);
    return psalist;
  }

  async deletePsaFile(itemId: bigint, psaId: string) {
    await axiosBase.delete(`${this.BACKENDHOST}/file/psa-check/${itemId}/${psaId}`);
  }
}

export const psaAPI = new PsaAPI();
