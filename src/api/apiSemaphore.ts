class ApiSemaphore {
  constructor() {
    this.requestingNewToken = false;
  }

  requestingNewToken: boolean;
}

export default new ApiSemaphore();