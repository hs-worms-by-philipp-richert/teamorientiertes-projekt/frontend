import axiosBase from './axiosBase';

const BACKENDHOST = import.meta.env.VITE_REACT_API_URL;

const fetchProductItemsWithPsa = async (page: number, take: number, searchStr?: string) => {
  const products = await axiosBase.get(
    `${BACKENDHOST}/psa/product-item/list?page=${page}&take=${take}${searchStr ? '&search=' + searchStr : ''}`
  );
  return products;
};

export default fetchProductItemsWithPsa;
