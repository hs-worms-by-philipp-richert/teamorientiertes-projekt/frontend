import { IProductManual } from "../../shared/interfaces/IProductManual";

class FileAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  getProductManual(manual: IProductManual) {
    if (!manual.ManualId || !manual.ProductId) return 'not-found';
    return `${this.BACKENDHOST}/file/product-manual/${manual.ProductId}/${manual.ManualId}`;
  }

  getProductImage(productId: bigint, productImageId: string) {
    return `${this.BACKENDHOST}/file/product-image/${productId}/${productImageId}`
  }

  getPSACheck(itemId: bigint, psaId: string) {
    return `${this.BACKENDHOST}/file/psa-check/${itemId}/${psaId}`
  }
}

export const fileAPI = new FileAPI();
