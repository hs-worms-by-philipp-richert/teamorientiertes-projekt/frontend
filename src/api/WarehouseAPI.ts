import axiosBase from './axiosBase';
import { StorageForm } from '../interfaces/storage-form.interface';
import { IWarehouseRegion } from '../../shared/interfaces/IWarehouseRegion';
import { IWarehouseStorage } from '../../shared/interfaces/IWarehouseStorage';
import { IWarehouseStorageTray } from '../../shared/interfaces/IWarehouseStorageTray';

class WarehouseAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async createWarehouse(warehouse: StorageForm) {
    const { data } = await axiosBase.post(`${this.BACKENDHOST}/warehouse`, warehouse, {
      headers: { 'Content-Type': 'application/json' },
    });
    return data;
  }

  async updateWarehouse(warehouseid: bigint, warehouse: StorageForm) {
    const response = await axiosBase.put(`${this.BACKENDHOST}/warehouse/${warehouseid}`, warehouse);
    return response;
  }

  async fetchWarehouses(page: number, take: number) {
    const warehouses = await axiosBase.get(`${this.BACKENDHOST}/warehouse/list?page=${page}&take=${take}`);
    return warehouses;
  }

  async fetchWarehouse(id: bigint) {
    const warehouse = await axiosBase.get(`${this.BACKENDHOST}/warehouse/${id}`);
    return warehouse;
  }

  async deleteWarehouse(warehouseId: bigint) {
    const response = await axiosBase.delete(`${this.BACKENDHOST}/warehouse/${warehouseId}`);
    return response;
  }

  async fetchRegions(page: number, take: number) {
    const trays = await axiosBase.get(`${this.BACKENDHOST}/warehouse-region/list?page=${page}&take=${take}`);
    return trays;
  }

  async createRegion(region: IWarehouseRegion) {
    const { data } = await axiosBase.post(`${this.BACKENDHOST}/warehouse-region`, region, {
      headers: { 'Content-Type': 'application/json' },
    });
    return data;
  }

  async deleteRegion(region: bigint) {
    const response = await axiosBase.delete(`${this.BACKENDHOST}/warehouse-region/${region}`);
    return response;
  }

  async createStorage(storage: IWarehouseStorage) {
    const { data } = await axiosBase.post(`${this.BACKENDHOST}/warehouse-storage`, storage, {
      headers: { 'Content-Type': 'application/json' },
    });
    return data;
  }

  async deleteStorage(storageid: bigint) {
    const response = await axiosBase.delete(`${this.BACKENDHOST}/warehouse-storage/${storageid}`);
    return response;
  }

  async fetchTrays(page: number, take: number) {
    const trays = await axiosBase.get(`${this.BACKENDHOST}/warehouse-storage-tray/list?page=${page}&take=${take}`);
    return trays;
  }

  async fetchTrayById(trayId: bigint) {
    const tray = await axiosBase.get(`${this.BACKENDHOST}/warehouse-storage-tray/${trayId}`);

    return tray;
  }

  async createTray(tray: IWarehouseStorageTray) {
    const { data } = await axiosBase.post(`${this.BACKENDHOST}/warehouse-storage-tray`, tray, {
      headers: { 'Content-Type': 'application/json' },
    });
    return data;
  }

  async deleteTrayById(trayId: bigint) {
    const response = await axiosBase.delete(`${this.BACKENDHOST}/warehouse-storage-tray/${trayId}`);
    return response;
  }
}

export const warehouseAPI = new WarehouseAPI();
