import axiosBase from './axiosBase';
import { OrderCheckout } from "../interfaces/order-checkout.interface"

const BACKENDHOST = import.meta.env.VITE_REACT_API_URL;

export const submitOrder = async (order: OrderCheckout) => {
    const response = await axiosBase.post(`${BACKENDHOST}/order`, order, {
        headers: { 'Content-Type': 'application/json' },
    })
    return response.data;
}