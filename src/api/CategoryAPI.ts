import axiosBase from './axiosBase';

class CategoryAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async createCategory(categoryname: string, categoryparent: number | null = null) {
    const { data } = await axiosBase.post(`${this.BACKENDHOST}/category`, {
      Name: categoryname,
      ParentCategoryId: categoryparent,
    });

    return data;
  }

  async deleteCategory(id: bigint) {
    await axiosBase.delete(`${this.BACKENDHOST}/category/${id}`);
  }

  async fetchCategories(showall: boolean = true, cascade: boolean = false) {
    const categories = await axiosBase.get(
      `${this.BACKENDHOST}/category/list/?showEmpty=${showall}&cascade=${cascade}`
    );
    return categories;
  }

  async fetchCategory(id: bigint, showall: boolean = true) {
    const category = await axiosBase.get(`${this.BACKENDHOST}/category/${id}?extended=${showall}`);
    return category;
  }

  async updateCategory(categoryid: bigint, categoryname: string, parent: bigint) {
    await axiosBase.put(
      `${this.BACKENDHOST}/category/${categoryid}`,
      { Name: categoryname, ParentCategoryId: parent },
      { headers: { 'Content-Type': 'application/json' } }
    );
  }

  async createProductCategory(productid: bigint, categoryid: bigint) {
    await axiosBase.post(
      `${this.BACKENDHOST}/product-category`,
      { ProductId: productid.toString(), CategoryId: categoryid.toString() },
      { headers: { 'Content-Type': 'application/json' } }
    );
  }

  async deleteProductCategoriesFromProduct(productid: bigint) {
    await axiosBase.delete(`${this.BACKENDHOST}/product-category/product/${productid}`);
  }

  deleteProductFromCategory = async (productid: bigint, categoryid: bigint) => {
    await axiosBase.delete(`${this.BACKENDHOST}/product-category/`, {
      headers: { 'Content-Type': 'application/json' },
      withCredentials: true,
      data: { ProductId: productid, CategoryId: categoryid },
    });
  };
}

export const categoryAPI = new CategoryAPI();
