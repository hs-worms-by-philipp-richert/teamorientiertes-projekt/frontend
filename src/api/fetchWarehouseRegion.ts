import axiosBase from './axiosBase';

const fetchWarehouseRegions = async (page: number, take: number) => {
  const trays = await axiosBase.get(
    `${import.meta.env.VITE_REACT_API_URL}/warehouse-region/list?page=${page}&take=${take}`
  );
  return trays;
};

export default fetchWarehouseRegions;
