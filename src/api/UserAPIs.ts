import axiosBase from './axiosBase';
import { IUser } from '../../shared/interfaces/IUser';

class UserAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async createUser(userdata: IUser) {
    const response = await axiosBase.post(`${this.BACKENDHOST}/user/register`, userdata, {
      headers: { 'Content-Type': 'application/json' },
    });
    return response;
  }

  async fetchAllUsers(
    page: number,
    take: number,
    searchParams: { search: string; sorting: 'ASC' | 'DESC' | string | undefined } = { search: '', sorting: 'ASC' },
    isVerified: boolean = false
  ) {
    const users = await axiosBase.get(
      `${this.BACKENDHOST}/user/list?page=${page}&take=${take}&search=${searchParams.search}&sort=${searchParams.sorting}&hasRequestedRoleUpdate=${isVerified}`
    );

    return users;
  }

  async deleteUser(userId: string) {
    await axiosBase.delete(`${this.BACKENDHOST}/user/${userId}`);
  }

  async getUserById(userId: string) {
    const result = await axiosBase.get(`${this.BACKENDHOST}/user/${userId}`);
    const user = result.data;
    delete user.password;

    return user;
  }

  async registerUser(formData: IUser, password: string = '') {
    const response = await axiosBase.post(
      `${this.BACKENDHOST}/user/create/?randompassword=${password === '' ? 'true' : 'false'}`,
      formData,
      {
        headers: { 'Content-Type': 'application/json' },
      }
    );

    return response;
  }

  putUserById = async (userId: string, formData: IUser) => {
    const response = await axiosBase.put(`${this.BACKENDHOST}/user/${userId}`, formData);

    return response;
  };

  putUserSelf = async (formData: IUser) => {
    const response = await axiosBase.put(`${this.BACKENDHOST}/user`, formData);

    return response;
  };

  async fetchUserRoles() {
    const roles = await axiosBase.get(`${this.BACKENDHOST}/role`);

    return roles;
  }

  async updateUsertoInternal(userId: string) {
    await axiosBase.put(`${this.BACKENDHOST}/user/update-role-to-internal/${userId}`);
  }

  async requestInternalPermissions() {
    await axiosBase.put(`${this.BACKENDHOST}/user/request-role-update`);
  }
}

export const userAPI = new UserAPI();
