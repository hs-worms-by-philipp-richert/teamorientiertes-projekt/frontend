import axios from 'axios';
import { getAuthToken, getAuthTokenExpired } from '../util/auth';
import ApiSemaphore from './apiSemaphore';

const BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
const axiosApiInstance = axios.create();
const axiosRefreshInstance = axios.create();

// intercept auth responses
axiosRefreshInstance.interceptors.response.use(
  (response) => {
    const setAccessToken: string | undefined = response.headers['x-set-access-token'];

    if (setAccessToken && setAccessToken.length > 0) {
      localStorage.setItem('_auth', setAccessToken);
      window.dispatchEvent(new Event('storage'));
      ApiSemaphore.requestingNewToken = false;
    }

    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// intercept api requests
axiosApiInstance.interceptors.request.use(
  async (config) => {
    const accessTokenOld = getAuthToken();

    if (accessTokenOld && getAuthTokenExpired() && !ApiSemaphore.requestingNewToken) {
      ApiSemaphore.requestingNewToken = true;

      await axiosRefreshInstance.get(`${BACKENDHOST}/auth/refresh`, {
        headers: { Authorization: `Bearer ${accessTokenOld}` },
        withCredentials: true,
      });

      ApiSemaphore.requestingNewToken = false;
    }

    const accessTokenNew = getAuthToken();

    if (accessTokenNew) {
      config.headers.Authorization = `Bearer ${accessTokenNew}`;
    }

    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// intercept api responses
axiosApiInstance.interceptors.response.use(
  (response) => {
    if (response.status === 401) {
      localStorage.removeItem('_auth');
      window.dispatchEvent(new Event('storage'));
    }

    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default axiosApiInstance;
