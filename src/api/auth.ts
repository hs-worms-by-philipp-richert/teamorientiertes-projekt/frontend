import axiosBase from './axiosBase';

const BACKENDHOST = import.meta.env.VITE_REACT_API_URL;

export const authuser = (login: {identifier: string, password: string}) => {
    const response = axiosBase.post(`${BACKENDHOST}/auth/login`, login,  { headers: { 'Content-Type': 'application/json' }, withCredentials: true });
    return response;
}