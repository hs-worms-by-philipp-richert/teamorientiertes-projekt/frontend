import axiosBase from './axiosBase';
import { IProductItem } from '../../shared/interfaces/IProductItem';

class ProductItemAPI {
  private BACKENDHOST: string;

  constructor() {
    this.BACKENDHOST = import.meta.env.VITE_REACT_API_URL;
  }

  async deleteProductItem(itemId: bigint): Promise<void> {
    try {
      await axiosBase.delete(`${this.BACKENDHOST}/product-item/${itemId}`);
    } catch (error) {
      console.error('Fehler beim Löschen des Produkts:', error);
      throw error;
    }
  }

  async fetchProductItems(productId: string): Promise<IProductItem[]> {
    try {
      const response = await axiosBase.get(`${this.BACKENDHOST}/product-item/product/${productId}`);
      return response.data;
    } catch (error) {
      console.error('Fehler beim Fetchen des Produktitems:', error);
      throw error;
    }
  }

  async postProductItem(ProductItemData: IProductItem): Promise<void> {
    try {
      await axiosBase.post(`${this.BACKENDHOST}/product-item/`, ProductItemData, {
        headers: { 'Content-Type': 'application/json' }
      });
    } catch (error) {
      console.error('Während der Erstellung des ProduktItems ist ein Fehler aufgetreten:', error);
      throw error;
    }
  }

  async updateProductItem(id: bigint, data: any) {
    try {
      const response = await axiosBase.put(`${this.BACKENDHOST}/product-item/${id}`, data, {
        headers: { 'Content-Type': 'application/json' },
        withCredentials: true
      });
      return response.data;
    } catch (error) {
      console.error('Während der Aktualisierung des Produktitems:', error);
      throw error;
    }
  }

  async fetchAvailable(productid: bigint | undefined, startdate: string, enddate: string) {
    try {
      const response = await axiosBase.get(`${this.BACKENDHOST}/product/${productid}/availability/${startdate}/${enddate}`);
      return response.data;
    } catch (error) {
      console.error('Beim Herunterladen der verfügbaren ProductItems', error);
      throw error;
    }
  }
}

export const productItemAPI = new ProductItemAPI();
