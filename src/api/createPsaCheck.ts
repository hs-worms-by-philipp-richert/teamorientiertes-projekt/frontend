import axiosBase from './axiosBase';

const BACKENDHOST = import.meta.env.VITE_REACT_API_URL;


const createPsaCheck = async (itemId: bigint, file: File) => {
  const formData = new FormData();
  formData.append('file', file);

  const result = await axiosBase.post(
    `${BACKENDHOST}/file/upload/psa-check/${itemId}`,
    formData,
    {
      headers: {
        "Content-Type": "multipart/form-data"
      },
    }
  );
  return result;
};

export default createPsaCheck;
