import React, { Suspense } from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import { Spin } from 'antd';
import './index.css'
import './i18n'


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Suspense fallback={<Spin className='flex h-lvh w-lvw justify-center items-center'></Spin>}>
    <App/>
    </Suspense>
  </React.StrictMode>,
)
