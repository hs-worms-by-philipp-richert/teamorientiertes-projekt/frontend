export const formatPrice = (price: number, locale: string = navigator.language, currency: string = 'EUR'): string => {
  return new Intl.NumberFormat(locale, {
    style: 'currency',
    currency: currency,
  }).format(price);
};

const dateOPtion: Intl.DateTimeFormatOptions = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
};

export const dateTimeFormator = (date: Date, locale: string = navigator.language): string => {
  return new Intl.DateTimeFormat(locale, dateOPtion).format(date);
};
