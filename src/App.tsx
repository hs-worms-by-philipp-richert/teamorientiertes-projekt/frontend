import Login from "./pages/Login.tsx";
import RootLayout from "./pages/RootLayout.tsx";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { ConfigProvider, Result } from "antd";
import { THEME } from "./constants/theme.ts";
import Inventory from "./pages/Inventory.tsx";
import StorageOverview from "./pages/StorageOverview.tsx";
import ItemPageContent from "./pages/ItemPageContent.tsx";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import CartContextProvider from "./store/cart-context.tsx";
import Cart from "./pages/Cart.tsx";
import Checkout from "./pages/Checkout.tsx";
import Users from "./pages/UserList.tsx";
import ProductOverview from "./pages/ProductOverview.tsx";
import Register from "./pages/Register.tsx";
import {
  checkAdminLoader,
  checkAuthLoader,
  checkWarehouseWorker,
} from "./util/auth.ts";
import ItemInventory from "./pages/ItemInventory.tsx";
import AuthContextProvider from "./store/auth-context.tsx";
import OrderOverview from "./pages/OrderOverview.tsx";
import OrderPageContent from "./pages/OrderPageContent.tsx";
import deDE from "antd/locale/de_DE";
import enUS from "antd/locale/en_US";
import dayjs from "dayjs";
import "dayjs/locale/de";
import OrderSuccess from "./pages/OrderSuccess.tsx";
import PSA from "./pages/PSA.tsx";
import ItemPSA from "./pages/ItemPSA.tsx";
import Orderhistory from "./pages/OrderHistory.tsx";
import NotFoundPage from "./pages/NotFoundPage.tsx";
import SettingsPage from "./pages/SettingsPage.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootLayout />,
    children: [
      { path: "/", element: <ProductOverview /> },
      { path: "/login", element: <Login /> },
      { path: "/register", element: <Register /> },
      { path: "/warenkorb", element: <Cart /> },
      { path: "/success", element: <OrderSuccess /> },
      {
        path: "/checkout",
        element: <Checkout />,
        loader: checkAuthLoader,
      },
      { path: "/produkt/:produktId", element: <ItemPageContent /> },
    ],
  },
  {
    path: "/dashboard",
    element: <RootLayout />,
    children: [
      { path: "/dashboard/user", element: <Users />, loader: checkAdminLoader },
      { path: "/dashboard/verlauf", element: <Orderhistory /> },
      {
        path: "/dashboard/bestellungen",
        element: <OrderOverview />,
        loader: checkWarehouseWorker,
      },
      {
        path: "/dashboard/lagerverwaltung",
        element: <StorageOverview />,
        loader: checkWarehouseWorker,
      },
      {
        path: "/dashboard/inventar",
        element: <Inventory />,
        loader: checkWarehouseWorker,
      },
      {
        path: "/dashboard/inventar/:productId",
        element: <ItemInventory />,
        loader: checkWarehouseWorker,
      },
      { path: "/dashboard/einstellungen", element: <SettingsPage /> },
      {
        path: "/dashboard/psa",
        element: <PSA />,
        loader: checkWarehouseWorker,
      },
      {
        path: "/dashboard/psa/:itemId",
        element: <ItemPSA />,
        loader: checkWarehouseWorker,
      },
      {
        path: "/dashboard/bestellungen/order/:orderId",
        element: <OrderPageContent />,
      },
    ],
  },
  {
    path: "*",
    element: <RootLayout />,
    errorElement: (
      <div className="h-dvh w-dvh flex justify-center items-center">
        <Result status="500" title="500" />
      </div>
    ),
    children: [{ path: "*", element: <NotFoundPage /> }],
  },
]);
const App = () => {
  const { i18n } = useTranslation();
  useEffect(() => {
    i18n.changeLanguage(navigator.language);
    dayjs.locale(i18n.language);
  }, []);

  return (
    <ConfigProvider
      theme={THEME}
      locale={i18n.language === "de-DE" ? deDE : enUS}
    >
      <AuthContextProvider>
        <CartContextProvider>
          <RouterProvider router={router}></RouterProvider>
        </CartContextProvider>
      </AuthContextProvider>
    </ConfigProvider>
  );
};

export default App;
