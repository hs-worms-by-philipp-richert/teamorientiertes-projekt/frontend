# React Frontend Server

## Setup the Project

1. Install npm packages with `npm install`
2. Initialize shared-submodule with `git submodule init` and `git submodule update`
3. Copy .env.dist and rename to .env
4. Start the Application with `npm run dev` to run as a Developer.

# Configuration

## Backend API

- In the `.env` you can change the `VITE_REACT_API_URL` value to connect to the backend.

## Theme Configuration

- You can change the primary-color of the components by changing the THEME constant in `src/constants/theme.ts`
- The Logo can be replaced by changing the path of 'logo' in `src/components/HeaderContent.tsx`

## Routing 

- You can find all defined routes in `src/App.tsx`.
- `/dashboard/*` can only be accessed by staff members and admins. These routes are restricted by using the shared permissions between  frontend and backend. 
- `/` can be accessed be all users.

## Authentication and Permissions

- Verification with JWT -> stored in LocalStorage
- The Authentication is handled via ContextAPI in `src/store/auth-context.tsx`. The Usertoken and the Userdata is managed there.
- In `src/util/auth.ts` there are several helper-functions to manage the JWT in LocalStorage.

# Deploying on Nginx Server

## Prerequisites

Before you begin, ensure you have the following:

- Node.js installed on your development machine.
- A React web application scaffolded using Vite.
- An Nginx server set up and running. You should have necessary permissions to deploy files to the server.

## Build the React Application

Build the React-Project using the following command:

```
npm run build
```

This command will generate a production-ready build of your React application in the dist directory.

## Configure Nginx

Next, you need to configure Nginx to serve your React application. Create a new server block configuration file or modify an existing one. Here's a basic example of an Nginx server block configuration:

````
server {
    listen 80;
    server_name example.com;

    location / {
        root /path/to/your/react/app/dist;
        try_files $uri /index.html;
    }
}

````

Replace `example.com` with your domain name and `/path/to/your/react/app/dist` with the path to the dist directory of your React application.

## Deploy the Application
After configuring Nginx, deploy your React application by copying the contents of the dist directory to your server. You can use SCP, FTP, or any other file transfer method you prefer.

````
scp -r /path/to/your/react/app/dist user@server:/path/to/destination
````

Replace `/path/to/your/react/app/dist` with the path to your local dist directory, `user` with your server username, server with your server IP address or domain name, and `/path/to/destination` with the path on your server where you want to deploy the application.

## Restart Nginx
Once the files are copied, restart Nginx to apply the changes:

````
sudo systemctl restart nginx
````